<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Speciality;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $redirectTo = '/login';

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Отображение формы регистрации
     */
    public function showRegistrationForm()
    {
        return view('app');
    }

    /**
     * Обработка регистрационного запроса
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return response()->json([ 'redirect' => $this->redirectPath() ]);
    }

    /**
     * Проверка наличия пользователя с введенным логином
     */
    public function userLoginExists(Request $request, $login)
    {
        $exists = User::where('login', '=', $login)->get()->first();

        return response()->json([ 'exists' => (bool)$exists ]);
    }

    /**
     * Обработка регистрационного запроса
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'surname' => 'required|string|max:50',
            'name' => 'required|string|max:50',
            'patronym' => 'required|string|max:50',
            'login' => 'required|string|min:2|max:50|unique:user',
            'password' => 'required|string|min:6|confirmed',
            'agreement' => 'accepted',
        ], [
            'required' => 'Поле обязательно к заполнению',
            'max' => 'Максимальная допустимая длина :max символов',
            'min' => 'Минимальная допустимая длина :min символов',
            'login.unique' => 'Пользователь с таким логином уже существует',
            'password.confirmed' => 'Не верное подтверждение пароля',
            'agreement.accepted' => 'Вы не дали согласие на обработку персональных данных',
        ]);
    }

    /**
     * Создание нового пользователя
     */
    protected function create(array $data)
    {
        return User::create([
            'surname' => $data['surname'],
            'name' => $data['name'],
            'patronym' => $data['patronym'],
            'login' => $data['login'],
            'password' => Hash::make($data['password']),
            'status_id' => 1,
        ]);
    }
}
