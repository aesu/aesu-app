<?php

namespace App\Http\Controllers\Database\conf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

use App\Models\conf\Member;
use App\Models\conf\Article;

class AppealController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Приглашение на конференцию
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получение страницы приглашения на участие
     *
     * GET /api/guest/conf/appeal
     */
    public function page(Request $request)
    {
        $request->validate(['year' => 'required']);

        $view = "other.conference.$request->year";

        if (!view()->exists($view)) {
            return $this->toJson();
        }

        if ($request->year <= date('Y')) {
            return View::make($view);
        }

        $articles = Article::whereYear('created_at', $request->year)->with('members')->get();

        $members = $articles->reduce(function ($red, $elem) {
            $red->push(...$elem->members);

            return $red;
        }, collect())
            ->unique('id');

        return View::make($view, [
            'articles' => $articles,
            'members' => $members,
        ]);
    }
}
