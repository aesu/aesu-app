<?php

namespace App\Http\Controllers\Database\conf;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

use App\Models\conf\Member;
use App\Models\conf\Status;
use App\Models\conf\Article;
use App\Models\conf\Document;

class ArticleGuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Статья на конференцию (для гостя)
    |--------------------------------------------------------------------------
    */

    /**
     * Создание статьи
     *
     * POST /api/guest/conf/article
     */
    public function create(Request $request)
    {
        $this->validateRequest($request);

        $article = Article::Create([
            'name' => $request->name,
            'section_id' => $request->section,
            'status_id' => Status::whereType(1)->get()->first()->id,
        ]);

        $documents = collect($request->file('documents'))->forPage(1, 10);

        foreach ($documents as $document) {
            $extension = $document->getClientOriginalExtension();
            $name = str_replace('.' . $extension, '', $document->getClientOriginalName());
            $path = 'conference/' . $article->id . '_' . $article->created_at->timestamp;

            Document::Create([
                'name' => $name,
                'extension' => $extension,
                'path' => $document->storeAs($path, $document->getClientOriginalName()),
                'article_id' => $article->id,
                'size' => $document->getSize(),
            ]);
        }

        $membersID = collect(json_decode($request->members))->forPage(1, 5)
            ->filter(function ($val) {
                return $val !== null && $val > 0;
            });

        $members = Member::whereIn('id', $membersID)->get();

        foreach ($members as $member) {
            $member->articles()->save($article);
        }

        $url = $request->getSchemeAndHttpHost() . '/conf/status?' . $article->id . '_' . $article->created_at->timestamp;

        return $this->toJson([
            'result' => true,
            'url' => $url,
        ]);
    }

    /**
     * Проверка верности заполнения полей статьи
     */
    protected function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required|between:3, 65535|unique:conf.article',
            'section' => 'required|between:1,7',
        ], [
            'required' => 'Поле обязательно к заполнению',
            'name.between' => 'Длина заголовка должна находится между :min - :max',
            'section.between' => 'Значение должно находится между :min - :max',
            'name.unique' => 'Статья с указанным наименованием уже существует',
        ]);

        $maxSize = 10 * 1024 * 1024;
        $documents = collect($request->file('documents'))->forPage(1, 10);

        foreach ($documents as $document) {
            if ($document->getSize() > $maxSize) {
                $name = $document->getClientOriginalName();

                return $this->toJson(['message' => 'Превышен допустимый размер документа \'' . $name . '\' в ' . ($maxSize / 1024 / 1024) . ' МБ'], 422);
            }
        }

        $hasWord = $documents->some(function ($elem) {
            $ext = $elem->getClientOriginalExtension();

            return in_array($ext, ['doc', 'docx']);
        });

        if (!$hasWord) {
            return $this->toJson(['message' => 'Хотя бы один документы должен быть формата Microsoft Word (.doc, .docx)'], 422);
        }

        return true;
    }

    /**
     * Получение информации о статье
     *
     * GET /api/guest/conf/article/status
     */
    public function status(Request $request)
    {
        $key = key($request->all());
        $inputs = explode('_', $key);

        if (!$key || count($inputs) !== 2)
            return $this->toJson(['message' => 'Не верно построен запрос'], 422);

        $ID = $inputs[0];
        $DATE = Carbon::createFromTimeStamp($inputs[1])->toDateTimeString();

        $article = Article::where([
            'id' => $ID,
            'created_at' => $DATE,
        ])
            ->get()
            ->first();

        if (!$article)
            return $this->toJson(['message' => 'Статья не найдена'], 422);

        $article->load([
            'status',
            'section',
            'members',
            'documents',
        ])
            ->setHidden([
                'status_id',
                'section_id',
            ]);

        $article->documents = $article->documents->each(function ($elem) {
            $elem->setHidden([
                'content',
                'article_id',
                'last_modified',
            ]);
        });

        return $this->toJson($article);
    }
}
