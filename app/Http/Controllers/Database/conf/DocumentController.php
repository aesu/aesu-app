<?php

namespace App\Http\Controllers\Database\conf;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\conf\Document;

class DocumentController extends Controller
{
    /**
     * Получение документов из базы данных конференции
     *
     * GET /document/conf
     */
    public function load(Request $request)
    {
        $key = key($request->all());
        $inputs = explode('_', $key);

        if (!$key || count($inputs) !== 2) {
            return view('errors.Error', ['message' => 'Запись о документе не найдена!']);
        }

        $ID = $inputs[0];
        $DATE = Carbon::createFromTimeStamp($inputs[1])->toDateTimeString();

        $document = Document::where([
            'id' => $ID,
            'last_modified' => $DATE,
        ])->get()->first();

        if (!$document) {
            return view('errors.Error', ['message' => 'Запись о документе не найдена!']);
        }

        if (!Storage::disk('public')->exists($document->path)) {
            return view('errors.Error', ['message' => 'Документ на локальном диске не найден!']);
        }

        $filename = Str::slug($document->name, '_') . '.' . $document->extension;

        return Storage::response($document->path, $filename);
    }
}
