<?php

namespace App\Http\Controllers\Database\log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\log\Laravel;

class LaravelController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Ошибки и Исключения Laravel
    |--------------------------------------------------------------------------
    */

    /**
     * Удаление сообщения об ошибке для Laravel
     *
     * DELETE /api/laravel/{id}
     */
    public function delete($id)
    {
        $log = Laravel::find($id);

        if (!$log)
            return $this->toJson(['message' => 'Запись не найдена'], 422);

        $log->delete();

        if (Laravel::all()->count() === 0)
            Storage::disk('logs')->put('laravel.log', '');

        return $this->toJson(['result' => true]);
    }

    /**
     * Получение списка сообщений об ошибке для Laravel
     *
     * GET /api/laravel/errors
     */
    public function errors()
    {
        $logs = Laravel::orderByDesc('count')->get();

        $logs = $logs->each(function ($elem) {
            $elem->trace = explode("\n", $elem->trace);
        });

        return $this->toJson($logs);
    }

    /**
     * Очистка всех сообщений об ошибке для Laravel
     *
     * DELETE /api/laravel/errors
     */
    public function clear()
    {
        Storage::disk('logs')->put('laravel.log', '');

        $logs = Laravel::query()->delete();

        return $this->toJson(['result' => true]);
    }
}
