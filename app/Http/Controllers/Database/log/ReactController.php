<?php

namespace App\Http\Controllers\Database\log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\log\React;

class ReactController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Ошибки и Исключения ReactJS
    |--------------------------------------------------------------------------
    */

    /**
     * Создание сообщения об ошибке для ReactJS
     *
     * POST /api/react
     */
    public function create(Request $request)
    {
        $request->validate([
            'url' => 'required',
            'message' => 'required',
            'trace' => 'required',
        ]);

        $logs = React::whereUrl($request->url)
            ->whereMessage($request->message)
            ->get();

        if ($logs->count() > 0) {
            React::whereId($logs->first()->id)
                ->increment('count');
        } else {
            React::Create([
                'url' => $request->url,
                'message' => $request->message,
                'trace' => $request->trace,
            ]);
        }

        return $this->toJson(['result' => true]);
    }

    /**
     * Удаление сообщения об ошибке для ReactJS
     *
     * DELETE /api/react/{id}
     */
    public function delete($id)
    {
        $log = React::find($id);

        if (!$log)
            return $this->toJson(['message' => 'Запись не найдена'], 422);

        $log->delete();

        return $this->toJson(['result' => true]);
    }

    /**
     * Получение списка сообщений об ошибке для ReactJS
     *
     * GET /api/react/errors
     */
    public function errors()
    {
        $logs = React::orderByDesc('count')->get();

        $logs = $logs->each(function ($elem) {
            $elem->trace = explode("\n", $elem->trace);
        });

        return $this->toJson($logs);
    }

    /**
     * Очистка всех сообщений об ошибке для ReactJS
     *
     * DELETE /api/react/errors
     */
    public function clear()
    {
        $logs = React::query()->delete();

        return $this->toJson(['result' => true]);
    }
}
