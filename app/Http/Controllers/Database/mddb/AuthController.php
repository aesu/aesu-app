<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\Models\mddb\Branch;
use App\Http\Requests\Auth\InfoUpdateRequest;
use App\Http\Requests\Auth\EmailUpdateRequest;
use App\Http\Requests\Auth\BranchUpdateRequest;
use App\Http\Requests\Auth\PasswordUpdateRequest;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Текущий пользователь
    |--------------------------------------------------------------------------
    */

    /**
     * Получение данных о текущем пользователе
     * только по CSRF, Sanctum токен работать не будет
     *
     * GET /auth
     */
    public function auth()
    {
        if (\Auth::guest()) {
            return $this->toJson();
        }

        $user = \Auth::user()->load([
            'userstatus',
            'permissions',
        ]);

        if ($user->branch) {
            $user->branch
                ->setHidden(['speciality_id'])
                ->speciality;
        }

        $user->permissions = $user->permissions->map(function ($elem) {
            return $elem->setHidden([
                'guard_name',
                'created_at',
                'updated_at',
                'pivot',
            ]);
        });

        $user->_csrf_token = csrf_token();

        return $this->toJson($user);
    }

    /**
     * Изменение ФИО и должности авторизированного пользователя
     *
     * POST /api/auth/info
     */
    public function info(InfoUpdateRequest $request)
    {
        $user = \Auth::user();

        $user->fill($request->only('surname', 'name', 'patronym', 'statusId'));

        $user->save();

        return $this->toJson([
            'result' => true,
            'surname' => $user->surname,
            'name' => $user->name,
            'patronym' => $user->patronym,
            'status' => $user->userstatus,
        ]);
    }

    /**
     * Изменение факультативной группы авторизированного пользователя
     *
     * POST /api/auth/branch
     */
    public function branch(BranchUpdateRequest $request)
    {
        $user = \Auth::user();

        if ($request->filled('branchId')) {
            $user->branch_id = $request->branchId;
        } else if ($request->has('branchId')) {
            $user->branch_id = null;
        }

        $user->save();

        return $this->toJson([
            'result' => true,
            'branch' => $user->branch,
        ]);
    }

    /**
     * Изменение email-адреса авторизированного пользователя
     *
     * POST /api/auth/email
     */
    public function email(EmailUpdateRequest $request)
    {
        $user = \Auth::user();

        $user->fill($request->only('email'));

        $user->save();

        return $this->toJson([
            'result' => true,
            'email' => $user->email,
        ]);
    }

    /**
     * Изменение пароля авторизированного пользователя
     *
     * POST /api/auth/password
     */
    public function password(PasswordUpdateRequest $request)
    {
        $user = \Auth::user();

        $user->password = Hash::make($request->password);

        $user->save();

        if (\Auth::guard('web')->check()) {
            \Auth::guard('web')->logout();

            $request->session()->invalidate();
        }

        return $this->toJson(['result' => true]);
    }

    /**
     * Получение списка пройденных тестов авторизированного пользователя
     *
     * GET  /api/auth/basket
     */
    public function basket()
    {
        $user = \Auth::user();

        if (!$user) {
            return $this->toJson(['message' => 'Пользователь не авторизован'], 422);
        }

        $baskets = $user->baskets()
            ->with(['test'])
            ->orderBy('passed_at')
            ->get();

        $baskets->each(function ($elem) {
            $elem->setHidden([
                'profile',
                'user_id',
                'test_id',
            ])
                ->test
                ->setHidden(['portrait']);
        });

        return $this->toJson($baskets);
    }
}
