<?php

namespace App\Http\Controllers\Database\mddb;

use App\Http\Classes\LCD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\mddb\Test;
use App\Models\mddb\Basket;
use App\Models\mddb\Result;
use App\Models\mddb\Quest;
use App\Models\mddb\Competence;

class BasketGuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Результат пройденного тестирования (для гостя)
    |--------------------------------------------------------------------------
    */

    /**
     * Результат прохождения теста в формате HTML
     *
     * GET /api/guest/basket/{id}/result-html
     */
    public function resultHTML($id)
    {
        $basket = Basket::find($id);

        if (!$basket)
            return $this->toJson(['message' => 'Результат прохождения теста не найден'], 422);

        $html = $basket->toHTML();

        return view('other.components.basket-result', [
            'quests' => $html,
            'basket' => $basket,
        ]);
    }

    /**
     * Расчет компетентносnного профиля по пройденному тесту
     *
     * GET /api/guest/basket/{id}/profile
     */
    public function profile(Request $request, $id)
    {
        $basket = Basket::find($id);

        if (!$basket) {
            return $this->toJson(['message' => 'Результат прохождения теста не найден'], 422);
        }

        $test = Test::find($basket->test_id);

        if (!$test) {
            return $this->toJson(['message' => 'Тест для результата прохождения теста не найден'], 422);
        }

        if (!$test->discipline) {
            return $this->toJson(['message' => 'Дисциплина для результата прохождения теста не найдена'], 422);
        }

        $competences = $test->discipline->competences;

        if ($competences->count() === 0) {
            return $this->toJson(['message' => 'Компетенций для дисциплины теста не найдено'], 422);
        }

        if ($request->has('update') || !$basket->profile) {
            #region СБОР ДАННЫХ

            $results = Result::whereBasketId($basket->id)->get();

            if ($results->count() === 0)
                return $this->toJson(['message' => 'Результаты по тестированию не найдены'], 422);

            $quests = $test->quests()
                ->with([
                    'answers',
                    'competences',
                    'questtype'
                ])
                ->orderBy('position')
                ->get();

            $questsAnswersID = $quests->map(function ($elem) {
                return $elem->answers->map(function ($value) {
                    return $value->id;
                });
            })->collapse();

            #endregion

            #region РАСЧЕТ ПРОФИЛЯ

            $LCD = new LCD();
            $LCD->setBelief($request->coef);

            $profile = $LCD->profile($competences, $quests, $results);

            $basket->profile = $profile->count() !== 0 ? $profile : null;
            $basket->save();

            #endregion

        }

        if (!$basket->profile) {
            return $this->toJson(['message' => 'Профиль для выбранного теста не рассчитан'], 422);
        }

        $profile = collect(json_decode($basket->profile));

        $competences->each(function ($elem) use ($profile) {
            $value = $profile->where('comp', $elem->id)->first();

            if (!$value)
                return false;

            $elem->setAttribute('v', true)
                ->setAttribute('val', $value->val)
                ->setAttribute('uc', $value->uc ?? null)
                ->setAttribute('mc', $value->mc ?? null)
                ->setHidden(['pivot']);
        });

        // dump($competences->each(function($elem) { dump($elem->id.' - '.$elem->val); }));

        return $this->toJson($competences);
    }

    /**
     * Получение списка пройденных тестов для методики УРК
     *
     * GET /api/guest/baskets
     */
    public function baskets(Request $request)
    {
        if (!$request->has(['yearPass', 'disciplineId'])) {
            return $this->toJson(['message' => 'Не все параметры заданы'], 422);
        }

        $baskets = Basket::query();

        if ($request->yearPass != 0) {
            $baskets->whereYear('passed_at', '=', $request->yearPass);
        }

        if (\Auth::check()) {
            $baskets = $baskets->with('user');
        }

        $disciplineId = (int) $request->disciplineId;

        $baskets = $baskets->with('test.discipline')
            ->get()
            ->filter(function ($elem) use ($disciplineId) {
                if (!$elem->test)
                    return false;
                if ($elem->test->option !== 1 || !$elem->test->discipline)
                    return false;

                $elem->setHidden([
                    'profile',
                    'user_id',
                    'test_id',
                ]);
                $elem->test
                    ->setHidden([
                        'discipline_id',
                        'portrait'
                    ]);

                return $elem->test->discipline->id === $disciplineId;
            })
            ->sortByDesc('passed_at')
            ->values();

        return $this->toJson($baskets);
    }

    /**
     * Расчет компетентностного профиля по пройденному тесту
     *
     * GET /api/basket/{id}/profile
     */
    public function CMKDprofile(Request $request, $id)
    {
        $basket = Basket::find($id);
        $quest1 = Quest::find($request->quest_id);
        $competence = Competence::find($request->competence_id);

        if (!$basket)
            return $this->toJson(['message' => 'Результат прохождения теста не найден'], 422);

        $test = Test::find($basket->test_id);

        if (!$test)
            return $this->toJson(['message' => 'Тест для результата прохождения теста не найден'], 422);

        #region СБОР ДАННЫХ
        $results = Result::whereBasketId($basket->id)->get();

        if ($results->count() === 0)
            return $this->toJson(['message' => 'Результаты по тестированию не найдены'], 422);

        $quests = $test->quests()
            ->with([
                'answers',
                'competences',
                'questtype'
            ])
            ->orderBy('position')
            ->get();


        #endregion

        #region РАСЧЕТ ПРОФИЛЯ
        $profiles = collect();
        $quests = $quests->filter(function ($elem) use ($competence) {
            return $elem->competences->contains('id', $competence->id);
        });

        foreach ($quests as $quest) {
            $LCD = new LCD();
            $LCD->setBelief($request->coef);
            $profile = $LCD->CMKDprofile($competence, $quest, $results, $test);
            $profiles->push($profile);
        }

        #endregion


        return $this->toJson($profiles);
    }


    /**
     * Расчет компетентностного профиля по одной компетенции на момент времени
     *
     * GET /api/basket/profiles
     */
    public function CMKDprofiles(Request $request)
    {
        $competence = Competence::find($request->competence_id);
        $user = User::find($request->user_id);

        $models = Basket::where('user_id', $request->user_id)
            ->whereHas('test', function ($query) use ($request) {
                $query->where('option', 1);
            })
            ->whereDate('passed_at', '<=', $request->date)
            ->orderByDesc('passed_at')
            ->select('id', 'test_id')
            ->get();

        $models = $models->unique('test_id');
        $m = collect();
        $profiles = collect();
        foreach ($models as $model)
            $m->push($model);
        foreach ($m as $model) # проход по тестам
        {

            $basket = Basket::find($model->id);
            if (!$basket)
                return $this->toJson(['message' => 'Результат прохождения теста не найден'], 422);
            $test = Test::find($basket->test_id);
            if (!$test)
                return $this->toJson(['message' => 'Тест для результата прохождения теста не найден'], 422);
            if (!$test->discipline)
                return $this->toJson(['message' => 'Дисциплина для результата прохождения теста не найдена'], 422);
            $edelement = $test->discipline->edelements->where('edelementtype_id', 3)->where('branch_id', $user->branch_id);
            if (count($edelement) !== 0) {

                $competences = $test->discipline->competences;
                #region СБОР ДАННЫХ
                $results = Result::whereBasketId($basket->id)->get();
                if ($results->count() === 0)
                    return $this->toJson(['message' => 'Результаты по тестированию не найдены'], 422);
                $quests = $test->quests()
                    ->with([
                        'answers',
                        'competences',
                        'questtype'
                    ])
                    ->orderBy('position')
                    ->get();

                #endregion
                #region РАСЧЕТ ПРОФИЛЯ
                $currentProfiles = collect();


                $questsC = $quests->filter(function ($elem) use ($competence) {
                    return $elem->competences->contains('id', $competence->id);
                });

                foreach ($questsC as $quest) {
                    $LCD = new LCD();
                    $LCD->setBelief($request->coef);
                    $profile = $LCD->CMKDprofile($competence, $quest, $results, $test);
                    $currentProfiles->push($profile);
                }
                $profiles->push($currentProfiles);
                #endregion
            }
        }

        return $this->toJson($profiles->collapse());
    }

}
