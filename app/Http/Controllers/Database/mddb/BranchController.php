<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\mddb\Branch;

class BranchController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Факультативные группы
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([  ]);
    }

    /**
     * Получение факультативной группы
     *
     * GET /api/branch/{id}
     */
    public function branch($id)
    {
        $branch = Branch::find($id);

        if (!$branch) {
            return $this->toJson(['message' => 'Факультативная группа не найдена'], 422);
        }

        $branch->load([
            'speciality',
            'users',
        ]);

        return $this->toJson($branch);
    }

    /**
     * Получение списка факультативных групп
     *
     * POST /api/branches
     */
    public function branches(Request $request)
    {
        $branches = Branch::query()
            ->when($request->has('name'), function ($query) use ($request) {
                $query->where('name', 'like', str_replace("*", "%", $request->name));
            })
            ->when($request->has('sort'), function ($query) use ($request) {
                switch ($request->sort) {
                    case 'd':
                        $query->orderByDesc('name');
                        break;
                    default:
                        $query->orderBy('name');
                        break;
                }
            })
            ->with('speciality')
            ->get();

        $branches = $this->modelsPaginator($branches, $request->perPage, $request->pageCount);

        return $this->toJson($branches);
    }
}
