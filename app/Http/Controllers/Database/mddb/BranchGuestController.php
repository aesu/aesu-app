<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\mddb\Branch;

class BranchGuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Факультативная группа (для гостя)
    |--------------------------------------------------------------------------
    */

    /**
     * Получение списка обучающихся, входящих в факультативную группу
     *
     * GET /api/guest/branch/{id}/users
     */
    public function users($id)
    {
        $branch = Branch::find($id);

        if (!$branch) {
            return $this->toJson(['message' => 'Факультативная группа не найдена'], 422);
        }

        $users = $branch->users()
            ->orderByRaw('surname')
            ->get();

        return $this->toJson($users);
    }

    /**
     * Получение списка факультативных групп
     *
     * GET /api/guest/branches
     */
    public function branches(Request $request)
    {
        $branches = Branch::query();

        if ($request->has('name'))
            $branches->where('name', 'like', str_replace("*", "%", $request->name));

        $branches = $branches->orderByDesc('name')->get();

        $branches = $this->modelsPaginator($branches, $request->perPage, $request->pageCount);

        return $this->toJson($branches);
    }
}
