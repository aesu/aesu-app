<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\mddb\Discipline;

class DisciplineController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Дисциплина
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получение дисциплины
     *
     * GET /api/discipline/{id}
     *
     * @param int
     */
    public function model($id)
    {
        $disc = Discipline::find($id);

        if (!$disc) {
            return $this->toJson(['message' => 'Дисциплина не найдена'], 422);
        }

        $disc->load([
            'userowner',
            'disciplinestatus',
        ])
            ->setHidden([
                'owner',
                'status',
            ]);

        return $this->toJson($disc);
    }

    /**
     * Получение списка дисциплин
     *
     * POST /api/disciplines
     *
     * @param \Illuminate\Http\Request
     */
    public function models(Request $request)
    {
        $discs = Discipline::query();

        if ($request->has('name')) {
            $discs->where('name', 'like', str_replace("*", "%", $request->name));
        }

        if ($request->has('shortname')) {
            $discs->where('shortname', 'like', str_replace("*", "%", $request->shortname));
        }

        $discs = $discs->with([
            'userowner',
            'disciplinestatus'
        ])
            ->get();

        $discs = $this->modelsPaginator($discs, $request->perPage, $request->pageCount);

        return $this->toJson($discs);
    }

    /**
     * Список компетенций для дисциплины
     *
     * GET /api/discipline/{id}competences
     *
     * @param int
     */
    public function competences($id)
    {
        $disc = Discipline::find($id);

        if (!$disc) {
            return $this->toJson(['message' => 'Дисциплина не найдена'], 422);
        }

        $competences = $disc->competences;

        if ($competences->count() === 0) {
            return $this->toJson(['message' => 'Компетенций для дисциплины не найдено'], 422);
        }

        return $this->toJson($competences);
    }

    /**
     * Список тестов для дисциплины
     *
     * GET /api/discpiline/{id}/tests
     *
     * @param int
     * @param \Illuminate\Http\Request
     */
    public function tests($id, Request $request)
    {
        $disc = Discipline::find($id);

        if (!$disc) {
            return $this->toJson(['message' => 'Дисциплина не найдена'], 422);
        }

        $tests = $disc->tests->where('option', $request->option)->first();

        if ($tests->count() === 0) {
            return $this->toJson(['message' => 'Тестов для дисциплины не найдено'], 422);
        }

        return $this->toJson($tests);
    }
}
