<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use App\Models\mddb\FileStorage;
use App\Http\Requests\ModelsRequest;

class FileStorageController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    // Получение файла
    // [ GET /api/filestorage/{id} ]
    public function filestorage($id)
    {
        $file = FileStorage::find($id);
        if (!$file)
            return $this->toJson(['message' => 'Не существует'], 404);
        return Storage::response($file->path, $file->name);
    }

    // Загрузка файла на сервер
    // [ POST /api/filestorage ]
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'file' => 'mimes:jpeg,bmp,png,pdf,jpg',
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ulid = Str::ulid();
            $path = 'didactic/';
            $path = $file->storeAs($path, $ulid);
            if ($path) {
                $model = FileStorage::create([
                    'name' => $request->name,
                    'path' => $path,
                ]);
                return $this->toJson($model->fresh());
            }
            return $this->toJson(['message' => 'Ошибка при сохранении файла'], 500);
        }
        return $this->toJson(['message' => 'Файл не был загружен'], 400);
    }

    // Удаление файла
    // [ DELETE /api/filestorage/{id} ]
    public function delete($id)
    {

        Validator::validate(['id' => $id], ['id' => 'exists:filestorage']);

        FileStorage::find($id)->delete();

        return $this->toJson(true);
    }

    //Список файлов без их содержимого
    // [ POST /api/files ]
    public function models(ModelsRequest $request)
    {
        $models = FileStorage::when($request->name, function ($query, $value) {
            $words = explode(' ', $value);

            foreach ($words as $word) {
                $query->where('name', 'like', '%' . $word . '%');
            }
        })
            ->select('id', 'name')
            ->orderBy('id');


        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);

    }
}