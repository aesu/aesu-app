<?php

namespace App\Http\Controllers\Database\mddb;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;

use App\Models\mddb\Lecture;
use App\Models\mddb\Document;
use App\Http\Classes\UrlFormat;
use App\Http\Requests\Lecture\ModelRequest;
use App\Http\Requests\Lecture\ExistsRequest;
use App\Http\Requests\Lecture\CreateRequest;

class LectureController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Лекции по дисциплинам
    |--------------------------------------------------------------------------
    */

    /**
     * GET /api/lecture/{id}
     *
     * @param ModelRequest
     * @param int
     */
    public function model(ModelRequest $request, int $id)
    {
        $model = Lecture::find($id);

        if (!$model) {
            return $this->toJson([
                'message' => 'Значение поля id не существует.'
            ], 422);
        }

        $model->load(array_filter($request['with']));

        return $this->toJson($model);
    }

    /**
     * Содержание лекции в формате HTML-документа
     *
     * GET /api/lecture/{id}/document-html
     *
     * @param int
     */
    public function documentHtml(int $id)
    {
        $model = Lecture::find($id);

        if (!$model) {
            return $this->toJson([
                'message' => 'Значение поля id не существует.'
            ], 422);
        }

        $document = $model->document;

        if (!$document || Storage::disk('public')->missing($document->path)) {
            return $this->toJson(
                ['message' => 'Документ не найден или не существует.'],
                422
            );
        }

        $content = Storage::disk('public')->get($document->path);

        return View::make('other.components.article-html', [
            'data' => json_decode($content, true),
        ]);
    }

    /**
     * POST /api/lecture
     *
     * @param ModelRequest
     */
    public function create(CreateRequest $request)
    {
        $inputs = $request->all();

        $slug = (new UrlFormat)->strToSlug($inputs['name']);
        $document = Document::createFromFile($inputs['document'], 'main');

        $model = new Lecture();

        $model->fill($inputs);
        $model->slug = substr($slug, 0, 255);
        $model->document_id = $document->id;

        $model->save();

        return $this->toJson($model);
    }

    /**
     * DELETE /api/lecture/{id}
     *
     * @param int
     */
    public function delete(int $id)
    {
        $model = Lecture::find($id);

        if (!$model) {
            return $this->toJson([
                'message' => 'Значение поля id не существует.'
            ], 422);
        }

        $document = $model->document;

        Storage::disk('public')->delete($document->path);

        $document->delete();
        $model->delete();

        return $this->toJson(['status' => true]);
    }

    /**
     * POST /api/lecture/exists
     *
     * @param ExistsRequest
     */
    public function exists(ExistsRequest $request)
    {
        $slug = (new UrlFormat)->strToSlug($request['name']);

        $count = Lecture::where('name', '=', $request['name'])
            ->orWhere('slug', '=', $slug)
            ->count();

        return $this->toJson([
            'exists' => $count > 0,
            'slug' => $slug,
        ]);
    }

    /**
     * POST /api/lectures
     *
     * @param ModelRequest
     */
    public function models(ModelRequest $request)
    {
        $models = Lecture::query()
            ->when($request->has('name'), function ($query) use ($request) {
                $query->where('name', 'like', "%$request->name%");
            })
            ->when($request->has('discipline_id'), function ($query) use ($request) {
                $query->where('discipline_id', '=', $request->discipline_id);
            })
            ->with(array_filter($request['with']));

        $models = $this->paginator($models, $request['perPage'], $request['pageCount']);

        return $this->toJson($models);
    }
}
