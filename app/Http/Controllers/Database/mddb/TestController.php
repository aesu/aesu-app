<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

use App\Models\mddb\EdElement;
use App\Models\mddb\Quest;
use App\Models\mddb\Test;
use App\Models\mddb\Basket;
use App\Models\mddb\Result;
use App\Http\Requests\ModelsRequest;
class TestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Анкета или Тест
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получение тестового банка
     *
     * GET /api/test/{id}
     */
    public function test(Request $request, $id)
    {
        $test = Test::find($id);

        if (!$test) {
            return $this->toJson(['message' => 'Тестовый банк заданий не найден'], 422);
        }

        $quests = collect();

        if ($request->edelement != null) {
            $eQuests = collect();
            $edelements = collect();
            $edelement = EdElement::find($request->edelement)
                ->load(['longquests', 'connections']);
            $edelements->push($edelement);
            /*if ($edelement->level == 4) {
                $edelement->connections->each(function ($elem) use ($edelements) {
                    if ($elem->external != 1) {
                        $edelement = EdElement::find($elem->edelement_to_id)
                            ->load('longquests');
                        $edelements->push($edelement);
                    }
                });
            }*/
            $edelements->each(function ($elem) use ($eQuests) {
                $eQuests = $eQuests->push($elem->longquests);
            });
            $quests = $eQuests->collapse();
        } else {
            $quests = $test->quests()
                ->with([
                    'answers',
                    'questtype'
                ])
                ->orderBy('position')
                ->get();
        }

        $quests->each(function ($elem) {
            $elem->setAttribute('quest_type', $elem->questtype->type);

            $elem->setHidden([
                'type',
                'questtype',
                'pivot',
            ]);
        });

        $test->load(['userowner', 'discipline'])
            ->setAttribute('quests', $quests)
            ->setAttribute('option_name', $test->option == 0 ? 'Анкетирование' : 'Тестирование')
            ->setHidden([
                'owner',
                'discipline_id'
            ]);

        return $this->toJson($test);
    }

    /**
     * Проверка доступа к тесту у авторизированного пользователя
     *
     * GET /api/test/{id}/access
     */
    public function hasAccess($id)
    {
        if (\Auth::guest())
            return $this->toJson(['access' => false]);

        $test = Test::find($id);

        if (!$test)
            return $this->toJson(['message' => 'Тестовый банк заданий не найден'], 422);

        $access = true;

        if (
            $test->access_till < Carbon::now() &&
            !\Auth::user()->hasAnyPermission(['admin', 'console'])
        )
            $access = false;

        return $this->toJson(['access' => $access]);
    }

    /**
     * Сохранение результатов прохождения тестирования
     *
     * POST /api/test/{id}/complete
     */
    public function complete(Request $request, $id)
    {
        $test = Test::find($id);

        if (!$test)
            return $this->toJson(['message' => 'Тестовый банк заданий не найден'], 422);

        $results = null;

        \DB::transaction(function () use ($request, $test, $results) {
            $data = [
                'user_id' => \Auth::user()->id,
                'test_id' => $test->id,
                'timer' => $request->timer,
                'score' => $request->score,
                'edelement_position' => $request->position,
            ];

            if ($request->filled('backDate')) {
                $data['passed_at'] = Carbon::createFromTimestamp((int) $request->backDate / 1000);
            }

            $basket = Basket::Create($data);

            $results = collect($request->results)->map(function ($elem) use ($basket) {
                $elem['basket_id'] = $basket->id;
                return $elem;
            })->toArray();

            Result::insert($results);
        });

        return $this->toJson(['result' => true]);
    }

    /**
     * Получение списка тестов
     *
     * GET /api/tests
     */
    public function tests(Request $request)
    {
        $tests = Test::query();

        if ($request->has('name'))
            $tests->where('name', 'like', str_replace("*", "%", $request->name));

        $tests = $tests->with([
            'quests',
            'userowner',
            'discipline',
        ])
            ->get();

        $tests = $this->modelsPaginator($tests, $request->perPage, $request->pageCount);

        $tests->each(function ($elem) {
            $elem->setAttribute('quest_count', $elem->quests->count());
            $elem->setAttribute('option_name', $elem->option == 0 ? 'Анкетирование' : 'Тестирование');

            $elem->setHidden([
                'quest',
                'owner',
                'discipline_id'
            ]);
        });

        return $this->toJson($tests);
    }

    /**
     * Получение списка тестов для пользователя,
     * которые доступны для текущего прохождения
     *
     * GET /api/tests-to-pass
     */
    public function testsToPass()
    {
        $user = \Auth::user()->load(['branch', 'subgroups']);

        $tests = Test::with([
            'quests',
            'discipline.branches',
            'subgroups',
            'userowner',
        ])
            ->where('access_till', '>', date('Y-m-d H:i:s'))
            ->orderBy('access_till')
            ->get();

        $tests = $tests->filter(function ($val, $key) use ($user) {
            if (!$user->branch)
                $inBranch = false;
            else
                $inBranch = $val->discipline ? $val->discipline->branches->contains('id', $user->branch->id) : false;

            $inGroups = $val->subgroups->filter(function ($val) use ($user) {
                return $user->subgroups->contains('id', $val->id);
            })->count() > 0;

            return $inBranch || $inGroups;
        })->values();

        $tests->each(function ($elem) {
            $elem->setAttribute('quest_count', $elem->quests->count());
            $elem->setAttribute('option_name', $elem->option == 0 ? 'Анкетирование' : 'Тестирование');

            $elem->setHidden([
                'quests',
                'owner',
                'discipline_id',
                'subgroups',
            ]);
        });

        return $this->toJson($tests);
    }

    /**
     * POST /api/tests
     */
    public function models(ModelsRequest $request)
    {
        $models = Test::when($request->name, function ($query, $value) {
            $words = explode(' ', $value);

            foreach ($words as $word) {
                $query->where('name', 'like', '%' . $word . '%');
            }
        })
            ->select(['id', 'name', 'option'])
            ->get();

        return $this->toJson($models);
    }

    /**
     * GET /api/test/{id}/quests
     */
    public function questslist($id)
    {
        $test = Test::find($id);

        if (!$test) {
            return $this->toJson(['message' => 'Не существует'], 200);
        }

        $quests = $test->quests()
            ->orderBy('position')
            ->get();

        return $this->toJson($quests);
    }
}
