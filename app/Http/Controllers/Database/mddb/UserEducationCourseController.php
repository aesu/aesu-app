<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\mddb\UserEducationCourse;
use App\Http\Requests\UserEducationCourse as Requests;

class UserEducationCourseController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    // Получение информации об образовательном курсе пользователя
    // [ GET /api/user/course/{id} ]
    public function usereducationcourse($id)
    {
        $course = UserEducationCourse::find($id)
            ->load('educationcourse');
        if (!$course)
            return $this->toJson(['message' => 'Не существует'], 200);
        return $course->toJson();
    }

    // Создание образовательного курса пользователя
    // [ POST /api/user/course/create ]
    public function create(Requests\CreateRequest $request)
    {
        $model = UserEducationCourse::create($request->validated());
        return $this->toJson($model->fresh());
    }

    // Изменение образовательного курса пользователя
    // [ PUT /api/user/course/{id} ]
    public function update(Requests\UpdateRequest $request, $id)
    {
        $model = UserEducationCourse::find($id)
            ->update($request->validated());
        return $this->toJson(true);
    }

    // Удаление образовательного курса пользователя
    // [ DELETE /api/user/course/{id} ]
    public function delete($id)
    {

        Validator::validate(['id' => $id], ['id' => 'exists:usereducationcourse']);

        UserEducationCourse::find($id)->delete();

        return $this->toJson(true);
    }

}