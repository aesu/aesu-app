<?php

namespace App\Http\Controllers\Other;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Testing\MimeType;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Intervention\Image\ImageManager;

use App\Models\mddb\Document;

class DevelopController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Отладка и другие методы разработчика
    |--------------------------------------------------------------------------
    */

    /**
     * GET /purpose-grid-only
     */
    public function grid(Request $request)
    {
        return view('develop.main');
    }

    /**
     * ANY /purpose-test-only
     */
    public function test(Request $request)
    {
        dump("Hello, World!");

        // $request->validate([
        //     'file.*' => 'required|file',
        // ]);

        // $allFiles = $request->allFiles();

        // foreach ($allFiles as $file) {
        //     $document = Document::createFromFile($file, 'main');

        //     dump($document->toArray());
        // }
    }

    /**
     * Запуск скрипта для FLM-builder
     *
     * POST /api/study/flm-builder
     */
    public function flmBuilder(Request $request)
    {
        /** Statistic of script execution */
        $_statKey = 'flm-builder-stat';
        $_stat = Cache::get($_statKey, [
            'total' => 0,
            'success' => 0,
            'errors' => 0,
            'last_exec_at' => now(),
        ]);

        $_stat['total']++;
        $_stat['last_exec_at'] = now();

        if (!$request->filled('zapros')) {
            Cache::put($_statKey, $_stat);

            return $this->toJson(['message' => 'Parameter zaproz is not defined'], 422);
        }

        if (!$request->file('file')) {
            Cache::put($_statKey, $_stat);

            return $this->toJson(['message' => 'File not passed'], 422);
        }

        $process = new Process([
            'python3',
            storage_path() . '/app/public/study/flm-builder/flm-builder.py',
            $request->input('zapros'),
            $request->file('file')->getPathName(),
        ]);

        $process->run();

        if (!$process->isSuccessful()) {
            $_stat['errors']++;

            Cache::put($_statKey, $_stat);

            return $this->toJson([
                'message' => 'Произошла ошибка во время исполнения скрипта',
                'error_output' => (new ProcessFailedException($process))->getMessage(),
            ], 422);
        }

        $_stat['success']++;

        Cache::put($_statKey, $_stat);

        return $process->getOutput();
    }
}
