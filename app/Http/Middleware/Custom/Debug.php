<?php

namespace App\Http\Middleware\Custom;

use Closure;

class Debug
{
    /*
    * Отображение Debugbar только для пользователей с правами разработчика
    */
    public function handle($request, Closure $next)
    {
        if (!\Auth::guest() && \Auth::user()->hasPermissionTo('develop')){
            \Debugbar::enable();
        }
        else {
            \Debugbar::disable();
        }

        return $next($request);
    }
}
