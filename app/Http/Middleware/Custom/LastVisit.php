<?php

namespace App\Http\Middleware\Custom;

use Closure;
use Illuminate\Support\Carbon;

class LastVisit
{
    /**
     * Отслеживание последнего посещения
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::guest()) {
            return $next($request);
        }

        if (\Auth::user()->last_visit->diffInMinutes(now()) >= 5){
            \DB::table('user')->where('id', \Auth::user()->id)->update(['last_visit' => Carbon::now()]);
        }

        return $next($request);
    }
}
