<?php

namespace App\Http\Requests\MethodEdElement;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'edelement_id' => 'exists:edelement,id|required',
            'method_id' => 'exists:method,id|required',
        ];
    }

}