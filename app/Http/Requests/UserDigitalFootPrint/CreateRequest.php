<?php

namespace App\Http\Requests\UserDigitalFootPrint;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'usereducationprofile_id' => 'exists:usereducationprofile,id',
            'page_from' => 'max:100',
            'page_to' => 'max:100',
            'page_component' => 'max:100',
            'actiondfp' => 'max:100',
            'parameters' => 'max:10000',
            //'created_at' => 'date|required',
        ];
    }

}
