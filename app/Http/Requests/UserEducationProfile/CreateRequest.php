<?php

namespace App\Http\Requests\UserEducationProfile;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'user_id' => 'exists:user,id',
            'edelement_id' => 'exists:edelement,id',
            'started_at' => 'date|required',
            'finished_at' => 'date|required',
        ];
    }

}
