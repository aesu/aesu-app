<?php

namespace App\Models\conf;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = null;

    protected $connection = 'conf';
    protected $table = 'article';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'section_id',
        'status_id',
        'created_at',
    ];

    protected $hidden = [

    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * Связь Многие к Одному с Section
     */
    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }

    /**
     * Связь Многие к Одному с Status
     */
    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    /**
     * Связь Один к Многие с Document
     */
    public function documents()
    {
        return $this->hasMany(Document::class, 'article_id');
    }

    /**
     * Связь Многие к Многие с Member
     */
    public function members()
    {
        return $this->belongsToMany(Member::class, 'memberlinkarticle', 'article_id', 'member_id');
    }
}
