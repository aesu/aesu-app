<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answer'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
        'position',
        'prime',
        'value',
        'minvalue',
        'maxvalue',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Одному с Quest
     */
    public function quest()
    {
        return $this->belongsTo(Quest::class, 'quest_id');
    }

    /**
     * Связь Один к Многим с Admatrix
     */
    public function admatrixes()
    {
        return $this->hasMany(Admatrix::class, 'answer_id');
    }

        /**
     * Связь Один к многим с PersonalPreference
     */
    public function personal()
    {
        return $this->hasMany(PersonalPreference::class, 'answer_id');
    }
}
