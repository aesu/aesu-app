<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    public $timestamps = true;

    const CREATED_AT = 'passed_at';
    const UPDATED_AT = null;

    protected $table = 'basket';

    protected $casts = [
        'passed_at' => 'datetime',
    ];

    protected $fillable = [
        'timer',
        'user_id',
        'test_id',
        'answer',
        'score',
        'profile',
        'passed_at',
        'edelement_position',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Одному с User
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * Связь Многие к Одному с Test
     */
    public function test()
    {
        return $this->belongsTo(Test::class, 'test_id');
    }

    /**
     * Связь Один к Многие с Result
     */
    public function results()
    {
        return $this->hasMany(Result::class, 'basket_id');
    }

    /**
     * Генерация результатов в HTML-формат
     */
    public function toHTML()
    {
        $test = $this->test;

        if (!$test) return $this->toJson([ 'message' => 'Тест для результата прохождения не найден' ], 422);

        $results = Result::whereBasketId($this->id)->get();

        if ($results->count() === 0) return $this->toJson([ 'message' => 'Результаты для данного прохождения не найдены' ], 422);

        $quests = $test->quests()
            ->with([
            'questtype',
            'answers',
        ])
            ->orderBy('position')
            ->get();

        $html = $quests->reduce(function($reducer, $elem) use ($results) {
            $resultsForQuest = $results->where('quest_id', $elem->id);

            $type = $elem->questtype->type;

            if (in_array($type, [0, 1, 2])) {
                $answers = $elem->answers
                    ->sortBy('position')
                    ->map(function($_elem) use ($resultsForQuest) {
                        if ($resultsForQuest->contains('answer_id', $_elem->id)) return '&#10004 <i>'.$_elem->name.'</i>';
                        else return $_elem->name;
                    });
            }
            else if ($type === 3) {
                $answers = collect([ $resultsForQuest->first()->value ]);
            }
            else if ($type === 4) {
                $answers = $elem->answers
                    ->sortBy('position')
                    ->map(function($_elem) use ($resultsForQuest) {
                        $resultForQuest = $resultsForQuest->where('answer_id', $_elem->id)->first();

                        return '['.$resultForQuest->value.'] '.$_elem->name;
                    });
            }

            if (!$answers) return $reducer;

            $reducer->push([
                'pos' => $elem->pivot->position,
                'name' => $elem->name,
                'answers' => $answers,
            ]);

            return $reducer;
        }, collect());

        return $html;
    }

    /**
     * Подсчет балловой оценки за пройденное тестирование
     */
    public function updateScore()
    {
        $test = $this->test;

        if (!$test) return $this->toJson(['message' => 'Тест для результата прохождения теста не найден'], 422);

        if ($test->option === 0) return $this->toJson(['message' => 'Количественная оценка анкетирования не производится'], 422);

        $results = Result::whereBasketId($this->id)->get();

        if ($results->count() === 0) return $this->toJson(['message' => 'Результаты по тестированию не найдены'], 422);

        $quests = $test->quests()->with([
            'answers',
            'questtype'
        ])
            ->get();

        $maxScore = 0;
        $currentScore = 0;

        foreach ($quests as $quest) {
            if ($quest->answers->count() === 0) continue;

            $testResult = $results->filter(function($elem) use ($quest) { return $elem->quest_id === $quest->id; });

            if ($quest->questtype->type === 0) $maxScore += 1;
            else {
                $values = $quest->answers->map(function($elem) { return $elem->value; })->toArray();
                $maxScore += max($values);
            }

            foreach ($quest->answers as $answer) {
                $contain = $testResult->contains(function($elem) use ($answer) { return $elem->answer_id === $answer->id; });

                if ($contain) {
                    $currentScore += $answer->value;
                }
            }
        }

        $this->score = number_format(100 * $currentScore / $maxScore, 1);
        $this->save();

        return $this->score;
    }
}
