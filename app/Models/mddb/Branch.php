<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branch';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'speciality_id',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Один к Многие с User
     */
    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'branch_id');
    }

    /**
     * Связь Многие к Одному с Speciality
     */
    public function speciality()
    {
        return $this->belongsTo(Speciality::class, 'speciality_id');
    }

    /**
     * Связь Многие к Многие с Discipline
     */
    public function disciplines()
    {
        return $this->belongsToMany(Discipline::class, 'branchlinkdisc', 'branch_id', 'discipline_id');
    }
}
