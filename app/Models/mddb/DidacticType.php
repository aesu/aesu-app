<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class DidacticType extends Model
{
    protected $table = 'type';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $hidden = [];

}