<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{
    protected $table = 'discipline'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
        'portrait',
        'owner',
        'shortname',
        'status_id',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Одному с User
     */
    public function userowner()
    {
        return $this->belongsTo(\App\Models\User::class, 'owner');
    }

    /**
     * Связь Многие к Одному с Disciplinestatus
     */
    public function disciplinestatus()
    {
        return $this->belongsTo(Disciplinestatus::class, 'status_id');
    }

    /**
     * Связь Многие к Многие с Branch
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'branchlinkdisc', 'discipline_id', 'branch_id');
    }

    /**
     * Связь Многие к Многие с Test
     */
    public function tests()
    {
        return $this->hasMany(Test::class, 'discipline_id');
    }

    /**
     * Связь Многие к Многие с Competence
     */
    public function competences()
    {
        return $this->belongsToMany(Competence::class, 'disclinkcomp', 'discipline_id', 'competence_id');
    }

    /**
     * Связь один ко многим с EdElement
     */
    public function edelements()
    {
        return $this->hasMany(EdElement::class, 'discipline_id');
    }
}
