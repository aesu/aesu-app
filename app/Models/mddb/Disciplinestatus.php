<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Disciplinestatus extends Model
{
    protected $table = 'disciplinestatus'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Один к Многие с Discipline
     */
    public function disciplines()
    {
        return $this->hasMany(Discipline::class, 'status_id');
    }
}
