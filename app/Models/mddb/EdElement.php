<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Hierarchy;
class EdElement extends Model
{
    use Hierarchy;

    protected $table = 'edelement';
    public $timestamps = false;

    protected $fillable = [
        'level',
        'order',
        'parent_id',
        'name',
        'shortname',
        'edelementtype_id',
        'control_id',
        'discform_id',
        'branch_id',
        'discipline_id',
    ];

    protected $hidden = ['pivot'];

    //Связь один к одному с DidacticDescription
    public function didacticdescription(){
        return $this->hasOne(DidacticDescription::class,'id','id');
    }
    
    //Связь многие к одному с EdElementType
    public function edelementtype(){
        return $this->belongsTo(EdElementType::class,'edelementtype_id', 'id' );
    }

    //Связь многие ко многим с EdElement (connections)
    public function connections(){
        return $this->belongsToMany(EdElement::Class, 'connection', 'edelement_from_id', 'edelement_to_id')->select(['connection_id','edelement_to_id', 'about', 'position', 'parent_id', 'name', 'external'])->with([
            implode('.', array_fill(0, 5, 'shortParent'))
        ]);
    }
    public function connections2(){
        return $this->belongsToMany(EdElement::Class, 'connection', 'edelement_from_id', 'edelement_to_id')->select(['connection_id','edelement_to_id', 'about', 'position', 'external']);
    }

    /**
     * Связь многие ко многим с Method
     */
    public function methods(){
        return $this->belongsToMany(Method::Class, 'edelementlinkmethod', 'edelement_id', 'method_id');
    }

    /**
     * Связь многие ко многим с Task
     */
    public function tasks(){
        return $this->belongsToMany(Task::Class, 'edelementlinktask', 'edelement_id', 'task_id');
    }

    /**
     * Связь многие ко многим с Quest
     */
    public function quests(){
        return $this->belongsToMany(Quest::Class, 'edelementlinkquest', 'edelement_id', 'quest_id')->withPivot('position');
    }
    public function longquests(){
        return $this->belongsToMany(Quest::Class, 'edelementlinkquest', 'edelement_id', 'quest_id')->withPivot('position')->with(['answers','questtype']);
    }


    /**
     * Связь многие к одному с Test
     */
    public function test(){
        return $this->belongsTo(Test::class,'test_id', 'id' );
    }

    /**
     * Связь многие к одному с Discipline
     */
    public function discipline(){
        return $this->belongsTo(Discipline::class, 'discipline_id', 'id');
    }

     /**
      * Связь многие к одному с Branch
      */
      public function branch(){
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
      }

      /**
     * Связь многие ко многим с Competence
     */
    public function competences(){
        return $this->belongsToMany(Competence::Class, 'edelementlinkcomp', 'edelement_id', 'competence_id');
    }
}