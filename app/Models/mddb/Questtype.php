<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Questtype extends Model
{    
    protected $table = 'questtype'; 
    public $timestamps = false;

    protected $fillable = [
        'type',
        'name',
        'shortname',
    ];

    protected $hidden = [

    ];
}
