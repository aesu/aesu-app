<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class UserEducationCourse extends Model{
    protected $table = 'usereducationcourse'; 
    public $timestamps = false;

    protected $fillable = [
        'usereducationprofile_id',
        'current_edelement_id',
        'educationcourse_id',
        'score',
        'finished',
    ];

    protected $hidden = [

    ];

    public function edelement(){
        return $this->belongsTo(Edelement::class,'current_edelement_id', 'id' );
    }

    public function usereducationprofile(){
        return $this->belongsTo(UserEducationProfile::class,'usereducationprofile_id', 'id');
    }

    public function educationcourse(){
        return $this->belongsTo(EducationCourse::class, 'educationcourse_id', 'id')->with('edelement');
    }
}