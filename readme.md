# ASEFU

Данная автоматизированная обучающая система разрабатывалась в рамках
диссертационного исследования по программе магистратуры высшей школы обучения
СФУ 2021 года выпуска. Проект реализует основные идеи и методики по выявлению на
первый взгляд скрытых и не совсем очевидных способностей как абитуриента на
этапах отборочных мероприятий, так и самого студента на всех этапах его
обучения.

## Подготовка окружения

Основной список ПО:

- [Git](https://git-scm.com/downloads) - использование текущего репозитория
- [Nginx](https://drive.google.com/drive/folders/1uZZ8szO8qAG8KcGlFw9JD8-7NzOBh964?usp=sharing) -
  настроенный локальный сервер и интерпретатор синтаксиса PHP
- [NodeJS](https://nodejs.org/en/) - интерпретация JavaScript через движок V8 +
  работа с пакетами JavaScript
- [Composer](https://getcomposer.org/download/) - менеджер работы с пакетами PHP
- [MySQL Server](https://dev.mysql.com/downloads/mysql/) - сервер для работы с
  базой данных MySQL

## Установка

> В качестве альтернативы можно использовать скрипт
> [aesfu-deploy.sh](https://gitlab.com/-/snippets/3762308) для авто
> развертывания проекта.

### Клонирование репозитория

Используя командную строку перейти в нужную директорию и выполнить команду:

```bash
git clone https://gitlab.com/aesfu/aesfu-app aesfu
```

> С использованием `Nginx` всю директорию проекта необходим разместить в
> `./html`

### Установка пакетов

В скачанном проекте, используя командную строку, установить необходимые пакеты
для `npm` и `composer`:

```bash
npm ci
composer install
```

### Подготовка проекта

- в корне проекта используя шаблон `.env.example`, создать файл `.env` и
  заполнить все пустые параметры
- выполнить команды:

  ```bash
  # сгенерировать уникальный ключи проекта
  php artisan key:generate

  # создать ссылку на директорию './storage' в './public'
  php artisan storage:link
  ```

- создать основную локальную базу данных MySQL из
  [шаблона структуры](https://gitlab.com/aesfu/aesu-database/-/blob/master/schemas/mddb/mddb.sql)

- в директории `./storage/logs/`, используя шаблон `laravel.sqlite.example`,
  создать файл `laravel.sqlite`

  ```bash
  cp storage/logs/laravel.sqlite.example storage/logs/laravel.sqlite
  ```

### Создание базы данных

- [запустить](https://dev.mysql.com/doc/refman/8.0/en/windows-start-command-line.html)
  на локальном компьютере MySQL Server
- установить MySQL Workbench и
  [подключиться](https://www.mysqltutorial.org/getting-started-with-mysql/connect-to-mysql-server/)
  к серверу `localhost` через логин и пароль `root` (пользователь по умолчанию)
- [импортировать](https://help.umbler.com/hc/en-us/articles/202385865-MySQL-Importing-Exporting-a-database)
  необходимые
  [схемы](https://gitlab.com/aesu/aesu-database/-/tree/master/schemas) используя
  любое окружение для работы с СУБД

## Вклад внесли

Научный руководитель и автор методик:

- Углев Виктор Александрович

Магистранты и методики:

- Пронин Артем Дмитриевич (2021)
  - Оценка уровня развития компетентностей (УРК)
  - Надпредметная оценка УРК
- Куклева Светлана Андреевна (2021)
  - Унифицированное графическое воплощение активности (УГВА) -
    [tyomka896/the-little](https://gitlab.com/tyomka896/the-little)
- Болсуновский Николай Александрович (2022)
  - Программа разработки продукционных экспертных систем с элементами нечёткой
    логики - [karunamaya/flm_react](https://github.com/karunamaya/flm_react)
- Смирнов Георгий Артемович
  - [cmkd/cmkd_front](https://gitlab.com/cmkd/cmkd_front)
  - [cmkd/cmkd_back](https://gitlab.com/cmkd/cmkd_back)
- Кузнецов Михаил Владимирович
  - [sfu3411527/practicefrontend](https://gitlab.com/sfu3411527/practicefrontend)

## Используемый стек технологий

##### Front-end:

- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) -
  управление web-страницами
- [React.js](https://reactjs.org)
  [v17.0^](https://reactjs.org/docs/getting-started.html) -
  JavaScript-библиотека для генерации пользовательских интерфейсов (UI)
- [Semantic UI React](https://react.semantic-ui.com) - фреймворк для создания UI
- [Material UI](https://mui.com) [v4.0^](https://v4.mui.com) - фреймворк для
  создания UI

##### Back-end:

- [PHP v8.1](https://www.php.net) - препроцессор написания HTML-кода
- [Laravel v10.0](https://laravel.com/docs/10.x) - PHP Framework для разработки
  веб-приложений
- [MySQL](https://www.mysql.com) - реляционная СУБД
- [SQLite](https://www.sqlite.org) - компактная встраиваемая система управления
  базами данных (СУБД)

## Контакты

- Пронин Артем Дмитриевич : `artempronin96@list.ru`
