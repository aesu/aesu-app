/**
 * Маршрутизатор для основных станиц
 */
import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import { Switch, Route } from 'react-router-dom'

import Wrapper from '../../containers/App.jsx'
import PlaceholderContent from './_components/PlaceholderContent/PlaceholderContent.jsx'

import './App.scss'

const Home = React.lazy(() => import('./Home/Home.jsx'))
const Exam = React.lazy(() => import('./Exam/Exam.jsx'))
const Method = React.lazy(() => import('./Method/Method.jsx'))
const Auth = React.lazy(() => import('./Auth/Auth.jsx'))
const Profile = React.lazy(() => import('./Profile/Profile.jsx'))
const Conference = React.lazy(() => import('./Conference/Conference.jsx'))

const App = () => (
    <Suspense fallback={<PlaceholderContent fluid grid />}>
        <Switch>
            <Route exact path="/" render={() => <Home />} />
            <Route exact path="/test*" render={() => <Exam />} />
            <Route exact path="/method*" render={() => <Method />} />
            <Route exact path={['/login', '/register']} render={() => <Auth />} />
            <Route exact path="/profile*" render={() => <Profile />} />
            <Route exact path="/conf*" render={() => <Conference />} />
        </Switch>
    </Suspense>
)

ReactDOM.render(<Wrapper component={App} />, document.getElementById('root'))
