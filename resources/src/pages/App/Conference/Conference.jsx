/**
 * Всероссийская научно-техническая конференция
 */
import React, { Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Grid } from 'semantic-ui-react'

import PlaceholderContent from '../_components/PlaceholderContent/PlaceholderContent.jsx'

const Appeal = React.lazy(() => import('./_routes/Appeal/Appeal.jsx'))
const Article = React.lazy(() => import('./_routes/Article/Article.jsx'))
const Member = React.lazy(() => import('./_routes/Member/Member.jsx'))
const Status = React.lazy(() => import('./_routes/Status/Status.jsx'))

const Conference = () => (
    <Grid stackable>
        <Suspense fallback={<PlaceholderContent fluid />}>
            <Switch>
                <Route exact path="/conf" render={() => <Appeal />} />
                <Route exact path="/conf/article" render={() => <Article />} />
                <Route exact path="/conf/member" render={() => <Member />} />
                <Route exact path="/conf/status" render={() => <Status />} />
            </Switch>
        </Suspense>
    </Grid>
)

export default Conference
