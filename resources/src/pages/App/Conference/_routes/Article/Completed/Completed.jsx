/**
 * Информирование об успешном создании статьи
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Divider, Segment, List } from 'semantic-ui-react'

const Completed = props => {
    const { url } = props.info

    return (
        <Grid.Row centered columns={ 1 }>
            <Grid.Column style={{ maxWidth: "650px" }}>
                <Segment>
                    <h3>Статья успешно создана</h3>
                    <p>
                        Статус обработки статьи модератором можно узнать по адресу:
                    </p>

                    <List bulleted>
                        <List.Item>
                            <a href={ url } target="_blank">{ url }</a>
                        </List.Item>
                    </List>

                    <Divider />

                    <NavLink to="/" className="ui basic button" activeClassName="">Завершить</NavLink>
                </Segment>
            </Grid.Column>
        </Grid.Row>
    )
}

export default Completed
