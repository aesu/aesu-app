/**
 * Создание участника конференции
 */
import React from 'react'
import { Grid, Form, Button, Dropdown, Segment, Checkbox, Divider } from 'semantic-ui-react'
import Datepicker from 'react-semantic-ui-datepickers'
import emailValidator from 'utils/emailValidator.jsx'
import axios from 'utils/axios.jsx'

import { APP_NAME } from 'utils/auth.jsx'

import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css'

class Member extends React.Component {
    state = {
        elements: {
            surname: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 50, trim: true },
            },
            name: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 50, trim: true },
            },
            patronym: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 50, trim: true },
            },
            status: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 100, trim: false },
            },
            degree: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 100, trim: false },
            },
            organization: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 255, trim: false },
            },
            city: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 255, trim: false },
            },
            email: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 100, trim: true },
            },
            format: {
                options: [
                    { key: 0, text: 'Очное', value: 0 },
                    { key: 1, text: 'Заочное', value: 1 },
                ],
                selected: 1,
            },
            pass: {
                value: false,
                valid: false,
            },
            birthday: {
                date: null
            },
            birthplace: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 255, trim: false },
            },
            registered: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 255, trim: false },
            },
            work: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 100, trim: false },
            },
            position: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 100, trim: false },
            },
            route: {
                value: '',
                error: null,
                valid: false,
                rules: { min: 1, max: 100, trim: false },
            },
            arrival: {
                date: null
            },
            hotel: {
                value: false,
                valid: false,
            },
            agreement: {
                value: false,
                valid: false,
            },
            rsci: {
                value: false,
                valid: false,
            },
        },
        isValid: false,
        loading: false,
    }

    componentDidMount() {
        document.title = `Заявка на участника конференции - ${APP_NAME}`
    }

    /**
     * Изменение полей ввода
     */
    inputChangeHandler = (_, { name, value }) => {
        const elements = { ...this.state.elements }
        const element = { ...elements[name] }

        element.value = element.rules.trim ? value.trim() : value
        element.error = null
        element.valid = false

        if (element.value.length === 0) element.error = `Поле обязательно к заполнению`
        else if (element.value.length < element.rules.min) element.error = `Минимальная длина ${element.rules.min} символов`
        else if (element.value.length > element.rules.max) element.error = `Максимальная длина ${element.rules.max} символов`
        else element.valid = true

        elements[name] = element

        this.setState({ elements: elements }, _ => this.isFormValid())
    }

    /**
     * Изменение поля email-адреса
     */
    emailChangeHandler = (_, { value }) => {
        const elements = { ...this.state.elements }
        const element = { ...elements.email }

        element.value = value.trim()
        element.error = null
        element.valid = false

        if (element.value.length === 0) element.error = `Поле обязательно к заполнению`
        else if (element.value.length > element.rules.max) element.error = `Максимальная длина ${element.rules.max} символов`
        else if (!emailValidator(element.value)) element.error = `Не верный формат email-адреса`
        else element.valid = true

        elements.email = element

        this.setState({ elements: elements }, _ => this.isFormValid())
    }

    /**
     * Изменение чек-боксов
     */
    checkboxChangeHandler = (_, { name, checked }) => {
        const elements = { ...this.state.elements }
        const element = { ...elements[name] }

        element.value = checked
        element.valid = checked

        elements[name] = element

        this.setState({ elements: elements }, _ => this.isFormValid())
    }

    /**
     * Изменение дат
     */
    dateChangedHandler = (_, { name, value }) => {
        const elements = { ...this.state.elements }
        const element = { ...elements[name] }

        element.date = value

        elements[name] = element

        this.setState({ elements: elements }, _ => this.isFormValid())
    }

    fromSubmitHandler = _ => {
        const { isValid } = this.state

        if (!isValid) return

        this.setState({ loading: true })

        const {
            surname,
            name,
            patronym,
            status,
            degree,
            organization,
            city,
            email,
            format,
            pass,
            birthday,
            birthplace,
            registered,
            work,
            position,
            route,
            arrival,
            hotel,
            agreement,
            rsci
        } = this.state.elements

        /** Если очное выступление и нет пропуска в город */
        const theFormat = format.selected === 0 && !pass.value

        const data = {
            surname: surname.value.trim(),
            name: name.value.trim(),
            patronym: patronym.value.trim(),
            status: status.value.trim(),
            degree: degree.value.trim(),
            organization: organization.value.trim(),
            city: city.value.trim(),
            email: email.value.trim(),
            format: format.selected,

            pass: pass.value,
            hotel: hotel.value,
            rsci: rsci.value,

            birthday: theFormat && birthday.date.getTime(),
            birthplace: theFormat && birthplace.value.trim(),
            registered: theFormat && registered.value.trim(),
            work: theFormat && work.value.trim(),
            position: theFormat && position.value.trim(),
            route: theFormat && route.value.trim(),
            arrival: theFormat && arrival.date.getTime(),
            agreement: theFormat && agreement.value,
        }

        axios.post('/api/guest/conf/member', data)
            .then(() => {
                alert('Заявка на участника конференции успешно сформирована')

                window.location.href = '/conf'
            })
            .catch(error => {
                const { elements } = this.state

                if (error.response.status === 422) {
                    const errors = error.response.data.errors

                    if (errors) {
                        Object.keys(errors).forEach(key => {
                            const element = elements[key]

                            element.valid = false
                            element.error = errors[key][0]

                            elements[key] = element
                        })
                    }
                }

                this.setState({
                    elements: elements,
                    isValid: false,
                    loading: false,
                })
            })
    }

    render() {
        const { elements, isValid, loading } = this.state
        const theFormat = elements.format.selected

        const extraForm = theFormat === 1 ? null : (
            <>
                <Divider />

                <label style={{ display: "block", textAlign: "center", fontWeight: "600", fontSize: "1.05rem" }}>
                    Данные для пропуска в закрытый город
                </label>

                <Segment style={{ background: "rgba(255, 250, 240, 0.3)" }}>
                    <Form.Field>
                        <Checkbox
                            name="pass"
                            checked={elements.pass.value}
                            onChange={this.checkboxChangeHandler}
                            label="Имеется оформленный пропуск в ЗАТО Железногорск на момент проведения конференции" />
                    </Form.Field>

                    {
                        !elements.pass.value &&
                        <>
                            <Form.Group>
                                <Datepicker
                                    name="birthday"
                                    label="Дата рождения"
                                    placeholder="Выбор даты"
                                    value={elements.birthday.date}
                                    autoComplete="off"
                                    locale="ru-RU"
                                    format="DD.MM.YYYY"
                                    firstDayOfWeek={1}
                                    clearable={false}
                                    showOutsideDays
                                    onChange={this.dateChangedHandler} />
                            </Form.Group>

                            <Form.Input
                                required
                                name="birthplace"
                                label="Место рождения"
                                value={elements.birthplace.value}
                                placeholder="Место рождения"
                                autoComplete="off"
                                error={elements.birthplace.error ? { content: elements.birthplace.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="registered"
                                label="Последнее место регистрации"
                                value={elements.registered.value}
                                placeholder="Последнее место регистрации"
                                autoComplete="off"
                                error={elements.registered.error ? { content: elements.registered.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="work"
                                label="Место работы"
                                value={elements.work.value}
                                placeholder="Место работы (организация)"
                                autoComplete="off"
                                error={elements.work.error ? { content: elements.work.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="position"
                                label="Должность"
                                value={elements.position.value}
                                placeholder="Должность"
                                autoComplete="off"
                                error={elements.position.error ? { content: elements.position.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="route"
                                label="Способ прибытия"
                                value={elements.route.value}
                                placeholder="Рейсовый автобус, личный автотранспорт (номер машины)"
                                autoComplete="off"
                                error={elements.route.error ? { content: elements.route.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Group>
                                <Datepicker
                                    name="arrival"
                                    label="Дата прибытия"
                                    placeholder="Выбор даты"
                                    value={elements.arrival.date}
                                    autoComplete="off"
                                    locale="ru-RU"
                                    format="DD.MM.YYYY"
                                    firstDayOfWeek={1}
                                    clearable={false}
                                    showOutsideDays
                                    onChange={this.dateChangedHandler} />
                            </Form.Group>
                        </>
                    }

                    <Form.Field>
                        <Checkbox
                            name="hotel"
                            checked={elements.hotel.value}
                            onChange={this.checkboxChangeHandler}
                            label="Необходимость бронирования места в гостинице" />
                    </Form.Field>

                    {
                        !elements.pass.value &&
                        <>
                            <Form.Field required>
                                <Checkbox
                                    name="agreement"
                                    checked={elements.agreement.value}
                                    onChange={this.checkboxChangeHandler}
                                    label="Я даю согласие на обработку своих персональных данных" />
                            </Form.Field>
                        </>
                    }

                </Segment>
            </>
        )

        return (
            <Grid.Row centered columns={1}>
                <Grid.Column style={{ maxWidth: "750px" }}>
                    <Segment>
                        <p style={{ textAlign: "center", fontSize: "1.15rem", textTransform: "uppercase" }}>
                            <b>Заявка на участника конференции</b>
                        </p>

                        <Divider />

                        <Form>
                            <Form.Input
                                required
                                name="surname"
                                label="Фамилия"
                                value={elements.surname.value}
                                placeholder="Фамилия"
                                autoComplete="off"
                                error={elements.surname.error ? { content: elements.surname.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="name"
                                label="Имя"
                                value={elements.name.value}
                                placeholder="Имя"
                                autoComplete="off"
                                error={elements.name.error ? { content: elements.name.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="patronym"
                                label="Отчество"
                                value={elements.patronym.value}
                                placeholder="Отчество"
                                autoComplete="off"
                                error={elements.patronym.error ? { content: elements.patronym.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="status"
                                label="Должность/уровень обучения"
                                value={elements.status.value}
                                placeholder="Бакалавр, специалист, магистрант, аспирант, инженер, профессор, доцент, научный работник"
                                autoComplete="off"
                                error={elements.status.error ? { content: elements.status.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="degree"
                                label="Ученая степень"
                                value={elements.degree.value}
                                placeholder="Ученая степень"
                                autoComplete="off"
                                error={elements.degree.error ? { content: elements.degree.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="organization"
                                label="Организация"
                                value={elements.organization.value}
                                placeholder="Организация"
                                autoComplete="off"
                                error={elements.organization.error ? { content: elements.organization.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="city"
                                label="Город"
                                value={elements.city.value}
                                placeholder="Город"
                                autoComplete="off"
                                error={elements.city.error ? { content: elements.city.error } : null}
                                onChange={this.inputChangeHandler} />

                            <Form.Input
                                required
                                name="email"
                                label="Email-адрес"
                                value={elements.email.value}
                                placeholder="Email-адрес"
                                autoComplete="off"
                                error={elements.email.error ? { content: elements.email.error } : null}
                                onChange={this.emailChangeHandler} />

                            {
                                false &&
                                <Form.Field required>
                                    <label>Форма участия</label>

                                    <Dropdown
                                        fluid
                                        selection
                                        selectOnBlur={false}
                                        placeholder="Форма участия"
                                        options={elements.format.options}
                                        value={elements.format.selected}
                                        onChange={(_, { value }) => this.setState(state => ({
                                            elements: {
                                                ...state.elements,
                                                format: {
                                                    ...state.elements.format,
                                                    selected: value,
                                                },
                                            }
                                        }), _ => this.isFormValid())} />
                                </Form.Field>
                            }

                            {
                                false &&
                                extraForm
                            }

                            <Form.Field required>
                                <Checkbox
                                    name="rsci"
                                    checked={elements.rsci.value}
                                    onChange={this.checkboxChangeHandler}
                                    label="Я проинформирован и даю согласие на индексацию своей публикации в РИНЦ" />
                            </Form.Field>

                            <Divider />

                            <Button
                                primary
                                loading={loading}
                                disabled={!isValid || loading}
                                style={{ width: "120px" }}
                                onClick={this.fromSubmitHandler} >Создать</Button>

                        </Form>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        )
    }

    /**
     * Проверка верности заполнения формы
     */
    isFormValid = _ => {
        let isValid = true

        /** Основная часть */
        const { surname, name, patronym, status, degree, organization, city, email, format, pass } = this.state.elements

        isValid = surname.valid &&
            name.valid &&
            patronym.valid &&
            status.valid &&
            degree.valid &&
            organization.valid &&
            city.valid &&
            email.valid

        /** Данные очного участия */
        if (isValid && format.selected === 0 && !pass.value) {
            const { birthday, birthplace, registered, work, position, route, arrival, agreement } = this.state.elements

            isValid = birthday.date &&
                birthplace.valid &&
                registered.valid &&
                work.valid &&
                position.valid &&
                route.valid &&
                arrival.date &&
                agreement.valid
        }

        /** Согласие на публикацию */
        if (isValid) {
            const { rsci } = this.state.elements

            isValid = rsci.valid
        }

        this.setState({ isValid: isValid })
    }
}

export default Member
