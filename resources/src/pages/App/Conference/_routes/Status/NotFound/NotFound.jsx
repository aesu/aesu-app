/**
 * 
 */
import React from 'react'
import { Grid, Segment } from 'semantic-ui-react'

const NotFound = () => (
    <Grid.Row centered columns={ 1 }>
        <Grid.Column style={{ maxWidth: "700px" }}>
            <Segment>
                <p>
                    К сожалению, статья не была найдена.{' '}
                </p>
                <p>
                    Возможно запрос простроен не верно или данные уже устарели.
                </p>
            </Segment>
        </Grid.Column>
    </Grid.Row>
)

export default NotFound
