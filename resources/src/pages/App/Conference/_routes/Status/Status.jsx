/**
 * Отслеживание статуса обработки статьи
 */
import React from 'react'
import { Grid, Table, Segment, List, Divider, Loader } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'

import { APP_NAME } from 'utils/auth.jsx'

import './Status.css'
import NotFound from './NotFound/NotFound'

class Status extends React.Component {
    state = {
        status: null,
        name: '',
        section: null,
        members: [],
        documents: [],
        notFound: false,
        loading: true,
    }

    componentDidMount() {
        document.title = `Статус обработки статьи - ${APP_NAME}`

        const status = window.location.search.replace('?', '')

        axios.get(`/api/guest/conf/article/status?${status}`)
            .then(({ data }) => {
                this.setState({
                    status: data.status,
                    name: data.name,
                    section: data.section,
                    members: data.members,
                    documents: data.documents,
                    loading: false
                })
            })
            .catch(() => {
                this.setState({
                    notFound: true,
                    loading: false,
                })
            })
    }

    render() {
        if (this.state.notFound) {
            return (<NotFound />)
        }

        const loader = <Loader inline active size="tiny" />

        const { status, name, section, members, documents, loading } = this.state

        const membersList = loading ? null : (
            <List ordered>
                {
                    members.map((elem, index) => (
                        <List.Item key={index}>
                            {`${elem.surname} ${elem.name} ${elem.patronym}`}
                        </List.Item>
                    ))
                }
            </List>
        )

        const documentsList = loading ? null : (
            <List ordered>
                {
                    documents.map((elem, index) => (
                        <List.Item key={index}>
                            {`${elem.name}.${elem.extension}`}
                        </List.Item>
                    ))
                }
            </List>
        )

        return (
            <Grid.Row centered columns={1}>
                <Grid.Column style={{ maxWidth: "700px" }}>
                    <Segment>
                        <p style={{ textAlign: "center", fontSize: "1.15rem" }}>
                            <b>Статус обработки статьи</b>
                        </p>

                        <Divider />

                        <Table id="article-status" basic="very" unstackable>
                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell style={{ width: "175px" }}><b>Статус</b></Table.Cell>
                                    <Table.Cell>
                                        {loading ? loader : status.name}
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell><b>Заглавие статьи</b></Table.Cell>
                                    <Table.Cell>
                                        {loading ? loader : name}
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell><b>Секция конференции</b></Table.Cell>
                                    <Table.Cell>
                                        {loading ? loader : `${section.name} (${section.example})`}
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell><b>Авторы статьи</b></Table.Cell>
                                    <Table.Cell>
                                        {loading ? loader : membersList}
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell><b>Приложенные документы</b></Table.Cell>
                                    <Table.Cell>
                                        {loading ? loader : documentsList}
                                    </Table.Cell>
                                </Table.Row>

                            </Table.Body>
                        </Table>

                    </Segment>
                </Grid.Column>
            </Grid.Row>
        )
    }
}


export default Status 
