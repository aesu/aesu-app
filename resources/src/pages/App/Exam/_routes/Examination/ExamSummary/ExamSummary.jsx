/**
 * Результат прохождения банка заданий
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Segment, Divider } from 'semantic-ui-react'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'

dayjs.extend(duration)

const ExamSummary = props => {
    const { discipline, test, score } = props
    const time = dayjs.duration(props.duration, 'seconds')

    return (
        <Grid.Column style={{ maxWidth: "750px" }}>
            <Segment>
                <h3>{ test.option_name } завершено</h3>
                <p>Время: { `${('0' + time.hours()).slice(-2)}:${('0' + time.minutes()).slice(-2)}:${('0' + time.seconds()).slice(-2)}` } </p>
                { test.option === 1 ? <p>Баллов: { score }/100</p> : null }
                <p>{ test.option === 0 ? 'Анкета' : 'Тест' }: { test.name }</p>
                <p>Дисциплина: { discipline ? discipline.name : '-' }</p>

                <Divider />

                <NavLink to="/tests" className="ui basic button" activeClassName="">К тестам</NavLink>
            </Segment>
        </Grid.Column>
    )
}

export default ExamSummary
