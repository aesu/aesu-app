import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Card, Divider } from 'semantic-ui-react'

import auth from 'utils/auth.jsx'

const NoTest = () => (
    <Grid.Column>
        <Card fluid style={{ maxWidth: "800px", margin: "auto", borderRadius: "0" }}>
            <Card.Content>
                <p>
                    На данный момент для вас нет никаких тестов для прохождения.
                </p>

                {
                    !auth().branch &&
                    <>
                        <Divider />
                        <p>
                            Замечено, что к вам не привязана ни одна учебная группа,{' '}
                            что может послужить причиной отсутствия отображения ваших тестов.{' '}
                            В настройках своего{' '}
                            <NavLink to="/profile/branch"><u>ПРОФИЛЯ</u></NavLink>{' '}
                            вы можете привязать себя к одной из групп.
                        </p>
                    </>
                }

            </Card.Content>
        </Card>
    </Grid.Column>
)

export default NoTest
