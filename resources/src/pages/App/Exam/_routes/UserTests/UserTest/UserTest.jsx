import React from 'react'
import { Card } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'

dayjs.extend(duration)

const UserTest = props => {
    const { test } = props

    const timer = test.timer ? dayjs.duration(test.timer, 's') : null
    const access_till = dayjs(test.access_till).format('DD.MM.YYYY HH:mm:ss')

    const portrait = !test.portrait ? null : [
        <p key="1"><b>Описание: </b></p>,
        <div key="2" dangerouslySetInnerHTML={{ __html: test.portrait }} />
    ]

    return (
        <Card fluid style={{ borderRadius: "0" }}>
            <Card.Content>
                <Card.Header>{ test.name }</Card.Header>
                <Card.Meta>{ test.option_name }</Card.Meta>
                <Card.Description>
                    <p>Вопросов: { test.quest_count }</p>
                    { timer ? <p>Время на выполнение: { timer.format('HH:mm:ss') }</p> : null }
                    { test.discipline ? <p>Дисциплина: { test.discipline.name }</p> : null }
                    <p>Тест доступен до: { access_till  }</p>

                    { portrait }
                </Card.Description>
            </Card.Content>
            <Card.Content extra textAlign="right">
                <NavLink className="ui blue basic small button" exact to={`/test/${test.id}`}>Начать</NavLink>
            </Card.Content>
        </Card>
    )
}

export default UserTest
