/**
 * Список карточек методик
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Card, Image } from 'semantic-ui-react'

import { APP_NAME } from 'utils/auth.jsx'

const _cards = [
    {
        href: "/method/ugva",
        image: "/storage/images/the_little.png",
        color: "yellow",
        header: "Метод УГВА",
        meta: "Куклева Светлана Андреевна",
        description: (
            <div className="description">
                <p>
                    Приводится иллюстрация визуализации учебного плана методом{' '}
                    <i>унифицированного графического воплощения активности</i>
                </p>
            </div>
        ),
    },
    {
        href: "/method/flm-builder",
        image: "/storage/images/flb-builder.png",
        color: "green",
        header: "FLM_Builder v4.b",
        meta: "Болсуновский Николай Александрович",
        description: (
            <div className="description">
                <p>
                    Программа разработки продукционных экспертных систем{' '}
                    с элементами нечёткой логики
                </p>
            </div>
        ),
    },
].map((elem, index) => (
    <Card
        key={index}
        href={elem.href}
        color={elem.color} >
        <Image
            wrapped
            ui={false}
            src={elem.image}
            alt={elem.header}
            style={{ minHeight: 256 }}
            onError={({ target }) => {
                target.onerror = null
                target.src = 'https://react.semantic-ui.com/images/wireframe/white-image.png'
            }} />

        <Card.Content>
            <Card.Header>{elem.header}</Card.Header>
            <Card.Meta>{elem.meta}</Card.Meta>
            <Card.Description>{elem.description}</Card.Description>
        </Card.Content>

        {elem.extra && <Card.Content>{elem.extra}</Card.Content>}
    </Card>
))

const cards = [
    {
        href: "/method/profile",
        image: "/storage/images/lcd_profile.png",
        color: " blue",
        header: "Оценка УРК",
        meta: "Пронин Артем Дмитриевич",
        description: (
            <div className="description">
                <p>
                    Рассматривается результат применения методики оценки{' '}
                    <i>уровня развития компетентностей</i>{' '}
                    по результатам пройденных банков тестовых заданий учеником{' '}
                </p>
            </div>
        )
    },
    {
        href: "/method/over-discipline",
        image: "/storage/images/over_lcd_profile.png",
        color: " pink",
        header: "Надпредметная оценка УРК",
        meta: "Пронин Артем Дмитриевич",
        description: (
            <div className="description">
                <p>
                    Развитие методики оценки уровня развития компетентностей{' '}
                    до <i>надпредметной оценки УРК</i> одного ученика и группы обучающихся
                </p>
            </div>
        )
    },
].map((elem, index) => (
    <NavLink
        exact
        key={index}
        to={elem.href ? elem.href : null}
        className={`ui card${elem.color || ''}`} >
        <Image
            wrapped
            ui={false}
            src={elem.image}
            alt={elem.header}
            style={{ minHeight: 256 }}
            onError={({ target }) => {
                target.onerror = null
                target.src = 'https://react.semantic-ui.com/images/wireframe/white-image.png'
            }} />

        <Card.Content>
            <Card.Header>{elem.header}</Card.Header>
            <Card.Meta>{elem.meta}</Card.Meta>
            <Card.Description>{elem.description}</Card.Description>
        </Card.Content>

        {elem.extra && <Card.Content>{elem.extra}</Card.Content>}
    </NavLink>
))

const Cards = () => {
    document.title = `Методики - ${APP_NAME}`

    return (
        <Grid.Column>
            <Card.Group centered>
                {cards}
                {_cards}
            </Card.Group>
        </Grid.Column>
    )
}

export default Cards
