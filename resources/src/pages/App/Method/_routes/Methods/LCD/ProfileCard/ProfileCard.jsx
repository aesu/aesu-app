/**
 * Компетентностный профиль ученика
 */
import React from 'react'
import { Divider } from 'semantic-ui-react'

import DataTable from './DataTable/DataTable.jsx'
import Card from './Card/Card.jsx'
import ResultHTML from './ResultHTML/ResultHTML.jsx'

const style = {
    title: {
        fontSize: "1.1em",
        fontWeight: "700"
    }
}

const ProfileCard = props => {
    const { profile, basket_id, displayChange, checkboxChange } = props

    return (
        <>
            <Divider />

            <label style={style.title}>Набор оцениваемых компетентностей:</label>

            <DataTable
                profile={profile}
                displayChange={displayChange}
                checkboxChange={checkboxChange} />

            <Divider />

            <label style={style.title}>Профиль без масштабирования:</label>
            <Card profile={profile} />

            <label style={style.title}>Масштабированный профиль:</label>
            <Card profile={profile} scaled={true} />

            <Divider />

            <ResultHTML basketId={basket_id} style={style} />
        </>
    )
}

export default ProfileCard
