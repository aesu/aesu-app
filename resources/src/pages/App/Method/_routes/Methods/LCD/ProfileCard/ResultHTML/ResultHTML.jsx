/**
 * Результаты прохождения тестирования
 */
import React from 'react'
import { Accordion, Icon, Segment } from 'semantic-ui-react'
import { Element, scroller } from 'react-scroll'
import axios from 'utils/axios.jsx'

import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'

class ResultHTML extends React.Component {
    state = {
        basket: null,
        loading: true,
        basketActive: false,
    }

    /**
     * Отображение ответов на пройденное тестирование
     */
    basketClickHandler = _ => {
        this.setState(state => ({
            basketActive: !state.basketActive
        }))

        if (!this.state.basket) {
            const { basketId } = this.props

            axios.get(`/api/guest/basket/${basketId}/result-html`)
                .then(({ data }) => {
                    this.setState({ basket: data, loading: false })

                    scroller.scrollTo('basket-result', { duration: 700, delay: 0, smooth: 'easeInOutQuart', offset: -50 })
                })
                .catch(() => {
                    this.setState({ loading: false })
                })
        }
    }

    render() {
        const { style } = this.props

        return (
            <Element name="basket-result">
                <Accordion>
                    <Accordion.Title
                        style={style.title}
                        active={this.state.basketActive}
                        onClick={this.basketClickHandler}>
                        <Icon name="dropdown" />
                        Оцениваемый банк заданий
                    </Accordion.Title>
                    <Accordion.Content active={this.state.basketActive}>
                        <Segment>
                            {
                                this.state.loading ?
                                    <PlaceholderContent fluid /> :
                                    <div dangerouslySetInnerHTML={{ __html: this.state.basket }}></div>
                            }
                        </Segment>
                    </Accordion.Content>
                </Accordion>
            </Element>
        )
    }
}

export default ResultHTML
