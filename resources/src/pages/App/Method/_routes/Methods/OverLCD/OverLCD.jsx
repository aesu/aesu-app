/**
 * Методика надпредметной оценки УРК
 */
import React from 'react'
import { Grid, Form, Segment, Button, Dropdown, Input, Divider } from 'semantic-ui-react'
import Datepicker from 'react-semantic-ui-datepickers'
import axios from 'utils/axios.jsx'
import dayjs from 'dayjs'

import auth, { APP_NAME } from 'utils/auth.jsx'

import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'
import ProfileCard from './ProfileCard/ProfileCard.jsx'

import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css'

const hasAccess = auth().canAny(['admin', 'console'])
const STORAGE_NAME = 'method_over'

class OverLCD extends React.Component {
    constructor() {
        super()

        const ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}

        this.state = {
            elements: {
                period: {
                    date: ls.period ? dayjs(ls.period).toDate() : dayjs().toDate(),
                },
                branch: {
                    options: [],
                    selected: null,
                    loading: true,
                },
                user: {
                    options: [],
                    selected: null,
                    loading: false,
                }
            },
            profile: null,
            loading: false,
        }
    }

    componentDidMount() {
        document.title = `Надпредметная оценка УРК - ${APP_NAME}`

        const ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}

        axios.get('/api/guest/branches')
            .then(({ data }) => {
                const branches = data.map(elem => ({
                    key: elem.id,
                    value: `${elem.id}`,
                    text: `${elem.name}`,
                }))

                this.setState(state => ({
                    elements: {
                        ...state.elements,
                        branch: {
                            options: branches,
                            selected: ls.branch_id || null,
                            loading: false,
                        }
                    }
                }), _ => this.requestUsers())
            }).catch(() => { })
    }

    /**
     * Выбор периода оценки
     */
    periodChangedHandler = (_, { value }) => {
        const elements = { ...this.state.elements }
        const { period } = elements

        const current = dayjs(period.date)
        const passed = dayjs(value)

        if (!passed.isValid()) return

        if (current.month() === passed.month() && current.year() === passed.year()) return

        const ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}
        ls.period = passed.valueOf()
        localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

        period.date = passed.toDate()
        elements.period = period

        this.setState({
            elements: elements,
            profile: null
        }, _ => {
            const userId = elements.user.selected

            if (userId) this.userChangeHandler(null, { value: userId })
        })
    }

    /**
     * Выбор факультативной группы
     */
    branchChangeHandler = (_, { value }) => {
        const ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}
        ls.branch_id = value
        localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

        const elements = { ...this.state.elements }
        const { branch, user } = elements

        branch.selected = value

        if (!value) {
            user.options = []
            user.selected = null
        }

        elements.branch = branch
        elements.user = user

        this.setState({
            elements: elements,
            profile: null,
        }, _ => this.requestUsers())
    }

    /**
     * Выбор пользователя(ей) для оценки
     */
    userChangeHandler = (_, { value, update }) => {
        const elements = { ...this.state.elements }
        const { user } = elements

        user.selected = value
        elements.user = user

        this.setState({
            elements: elements,
            profile: null
        })

        if (!value) return

        const { period, branch } = elements

        const periodAt = period.date
        const branchId = branch.selected

        if (!periodAt || !branchId) return

        this.setState({ loading: true })

        axios.get(`/api/guest/over-discipline/profile?periodAt=${periodAt.getTime()}&branchId=${branchId}&userId=${value}${update ? '&update' : ''}`)
            .then(({ data }) => {
                this.setState({
                    profile: data.length ? data : null,
                    loading: false,
                })
            })
            .catch(() => {
                this.setState({ loading: false })
            })

    }

    /**
     * Отображения названия оси диаграммы
    */
    displayChangeHandler = (event, index) => {
        let profile = [...this.state.profile]

        profile[index].display = event.target.value

        this.setState({ profile: profile })
    }

    /**
     * Отображение оси диаграммы
    */
    checkboxChangeHandler = (_, { checked }, index) => {
        let profile = [...this.state.profile]

        profile[index].v = checked

        this.setState({ profile: profile })
    }

    render() {
        const { elements, profile } = this.state

        let update = null
        const selected = elements.user.selected

        if (hasAccess && selected && profile) {
            update = (
                <Button
                    basic
                    style={{ margin: "0 0 0 14px" }}
                    onClick={event => this.userChangeHandler(event, { value: selected, update: true })}>
                    Обновить
                </Button>
            )
        }

        let profileCard = null

        if (this.state.loading) profileCard = (<> <Divider /> <PlaceholderContent fluid /> </>)
        else if (this.state.profile) profileCard = (
            <ProfileCard
                profile={profile}
                displayChange={this.displayChangeHandler}
                checkboxChange={this.checkboxChangeHandler} />
        )

        return (
            <Grid.Column style={{ maxWidth: "800px" }}>
                <Segment>
                    <Form>
                        <Form.Group>
                            <Datepicker
                                showOutsideDays
                                clearable={false}
                                locale="ru-RU"
                                format="*.MM.YYYY"
                                firstDayOfWeek={1}
                                datePickerOnly={true}
                                placeholder="Выбор даты"
                                label="Период до"
                                value={elements.period.date}
                                onChange={this.periodChangedHandler} />
                        </Form.Group>

                        <Form.Field>
                            <label>Факультативная группа</label>
                            <Dropdown
                                fluid
                                search
                                selection
                                clearable
                                selectOnBlur={false}
                                placeholder="Факультативные группы"
                                noResultsMessage="Ничего не найдено"
                                loading={elements.branch.loading}
                                options={elements.branch.options}
                                value={elements.branch.selected}
                                onChange={this.branchChangeHandler} />
                        </Form.Field>

                        <Form.Field>
                            <label>Студент</label>
                            <Input>
                                <Dropdown
                                    fluid
                                    search
                                    selection
                                    clearable
                                    selectOnBlur={false}
                                    placeholder="Студенты"
                                    noResultsMessage="Ничего не найдено"
                                    loading={elements.user.loading}
                                    options={elements.user.options}
                                    value={elements.user.selected}
                                    onChange={this.userChangeHandler} />
                                {update}
                            </Input>
                        </Form.Field>
                    </Form>

                    {profileCard}
                </Segment>
            </Grid.Column>
        )
    }

    /**
     * Получение пользователей
     */
    requestUsers = () => {
        const elements = { ...this.state.elements }
        const { branch } = elements

        const branchId = branch.selected

        if (!Boolean(branchId)) return

        this.setState(state => ({
            elements: {
                ...state.elements,
                user: {
                    options: [],
                    selected: null,
                    loading: true,
                }
            }
        }))

        axios.get(`/api/guest/branch/${branchId}/users`)
            .then(({ data }) => {
                let options = data.map((elem, index) => {
                    const userName = hasAccess ? `${elem.surname} ${elem.name} ${elem.patronym}` : `Студент №${index + 1}`

                    return {
                        key: elem.id,
                        value: `${elem.id}`,
                        text: `${userName}`
                    }
                })

                options.unshift({
                    key: 0,
                    value: `0`,
                    text: `Групповой профиль`
                })

                this.setState(state => ({
                    elements: {
                        ...state.elements,
                        user: {
                            ...state.elements.user,
                            options: options,
                            loading: false,
                        }
                    }
                }))
            })
            .catch(error => {
                this.setState(state => ({
                    elements: {
                        ...state.elements,
                        user: {
                            ...state.elements.user,
                            loading: false,
                        }
                    },
                    loading: false,
                }))
            })
    }
}

export default OverLCD
