/**
 * Межпредметный профиль ученика
 */
import React, { Component } from 'react'
import { Table, Checkbox, Divider, Popup } from 'semantic-ui-react'

import Card from './Card/Card.jsx'
import DataTable from './DataTable/DataTable.jsx'

const style = {
    title: {
        fontSize: "1.1em",
        fontWeight: "700"
    }
}

class ProfileCard extends Component {
    render() {
        const { profile, displayChange, checkboxChange } = this.props

        return (
            <>
                <Divider />

                <label style={style.title}>Набор оцениваемых компетентностей:</label>

                <DataTable
                    profile={profile}
                    displayChange={displayChange}
                    checkboxChange={checkboxChange} />

                <Divider />

                <label style={style.title}>Компетентностный профиль:</label>
                <Card profile={profile} />
            </>
        )
    }
}

export default ProfileCard
