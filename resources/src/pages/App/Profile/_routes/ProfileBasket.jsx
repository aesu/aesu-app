/**
 * Отображение пройденных банков заданий
 */
import React from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Grid, Table, Breadcrumb, Icon, Divider, Placeholder } from 'semantic-ui-react'
import { School } from '@material-ui/icons'
import axios from 'utils/axios.jsx'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'

dayjs.extend(duration)

import { APP_NAME } from 'utils/auth.jsx'
import store, { ProfileTypes } from 'store/store.jsx'

class ProfileBasket extends React.Component {
    componentDidMount() {
        document.title = `Пройденные тесты - ${APP_NAME}`

        const { baskets } = this.props.profile

        if (!baskets.loaded) {
            axios.get(`/api/auth/basket`)
                .then(({ data }) => {
                    store.dispatch(ProfileTypes.baskets(data))
                })
                .catch(error => {
                    store.dispatch(ProfileTypes.baskets())
                })
        }
    }

    render() {
        const { baskets } = this.props.profile

        const body = !baskets.loaded ? (
            Array(5).fill(0).map((_, index) => (
                <Table.Row key={index}>
                    <Table.Cell><Placeholder><Placeholder.Line /></Placeholder></Table.Cell>
                    <Table.Cell>
                        <Placeholder style={{ width: '70%' }}>
                            <Placeholder.Line />
                        </Placeholder>
                    </Table.Cell>
                    <Table.Cell><Placeholder><Placeholder.Line /></Placeholder></Table.Cell>
                    <Table.Cell><Placeholder><Placeholder.Line /></Placeholder></Table.Cell>
                    <Table.Cell></Table.Cell>
                    <Table.Cell></Table.Cell>
                </Table.Row>
            ))
        ) : (
            baskets.data.map((elem, index) => {
                const timer = dayjs.duration(elem.timer, 's')
                const passed_at = dayjs(elem.passed_at)

                const link = elem.test.option === 0 ? null : (
                    <NavLink
                        to={`/method/profile?y=${passed_at.year()}&d=${elem.test.discipline_id}&b=${elem.id}`}
                        title="Компетентностный профиль"
                    >
                        <School style={{ color: '#1b1c1d' }} />
                    </NavLink>
                )

                return (
                    <Table.Row key={index}>
                        <Table.Cell textAlign="center">{index + 1}</Table.Cell>
                        <Table.Cell>{elem.test.name}</Table.Cell>
                        <Table.Cell>{timer.format('HH:mm:ss')}</Table.Cell>
                        <Table.Cell>{passed_at.format('DD.MM.YYYY HH:mm')}</Table.Cell>
                        <Table.Cell textAlign="right">
                            {elem.test.option === 1 && `${elem.score || ' - '}/100`}
                        </Table.Cell>
                        <Table.Cell textAlign="center" style={{ paddingLeft: 0 }}>{link}</Table.Cell>
                    </Table.Row>
                )
            })
        )

        return (
            <Grid.Row columns={1}>
                <Grid.Column verticalAlign="middle">
                    <div className="button-toggle" onClick={this.props.onToggle}><Icon name="bars" /></div>

                    <Breadcrumb
                        size="tiny"
                        icon="right angle"
                        sections={[
                            { key: 0, content: 'Профиль', to: '/profile', as: (NavLink), activeClassName: '' },
                            { key: 1, content: 'Пройденные тесты', link: false },
                        ]} />
                </Grid.Column>

                <Divider />

                <Grid.Column>
                    <Table basic="very" unstackable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell textAlign="center" style={{ width: "30px" }}>#</Table.HeaderCell>
                                <Table.HeaderCell>Наименование</Table.HeaderCell>
                                <Table.HeaderCell style={{ width: "100px" }}>Время</Table.HeaderCell>
                                <Table.HeaderCell style={{ width: "150px" }}>Пройден</Table.HeaderCell>
                                <Table.HeaderCell style={{ width: "80px" }}>Баллов</Table.HeaderCell>
                                <Table.HeaderCell style={{ width: "32px" }}></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {body}
                        </Table.Body>
                    </Table>
                </Grid.Column>
            </Grid.Row>
        )
    }
}

const mapStateToProps = state => ({ profile: state.profile })

export default connect(mapStateToProps)(ProfileBasket)
