/**
 * Настройки email-адреса
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Form, Breadcrumb, Icon, Divider } from 'semantic-ui-react'
import emailValidator from 'utils/emailValidator.jsx'
import axios from 'utils/axios.jsx'

import auth, { APP_NAME } from 'utils/auth.jsx'

class ProfileEmail extends React.Component {
    state = {
        elements: {
            email: {
                value: "",
                error: null,
                valid: false,
                rules: {
                    min: 0,
                    max: 255
                }
            }
        },
        loading: false,
        valid: false
    }

    componentDidMount() {
        document.title = `Настройка почты - ${APP_NAME}`

        const elements = { ...this.state.elements }

        elements.email.value = auth().email

        this.setState({ elements: elements })
    }

    /**
     * Изменение поля ввода
     */
    inputChangeHandler = (event, name) => {
        const elements = { ...this.state.elements }
        const element = { ...elements[name] }

        element.value = event.target.value.trim()
        element.error = null
        element.valid = false

        if (element.value.length === 0) element.error = `Поле обязательно к заполнению`
        else if (element.value.length > element.rules.max) element.error = `Максимальная длина ${element.rules.max} символов`
        else if (!emailValidator(element.value)) element.error = `Не верный формат email-адреса`
        else element.valid = true

        elements[name] = element

        this.setState({
            elements: elements,
            valid: this.isValid(elements)
        })
    }

    /**
     * Инициирование формы
     */
    submitFormHandler = () => {
        this.setState({ loading: true })

        const { elements } = this.state

        const data = {
            email: elements.email.value,
        }

        axios.put('/api/auth/email', data)
            .then(({ data }) => {
                if (data.result) {
                    auth().email = data.email
                }

                this.setState({
                    loading: false,
                    valid: false,
                })
            })
            .catch(({ response }) => {
                const errors = response.data.errors

                const keys = Object.keys(errors)

                if (keys.length) {
                    for (let key of keys) {
                        elements[key].error = errors[key]
                        elements[key].valid = false
                    }

                    this.setState({ loading: false, elements: elements })
                }
            })
    }

    render() {
        const { elements } = this.state

        return (
            <Grid.Row columns={1}>
                <Grid.Column verticalAlign="middle">
                    <div className="button-toggle" onClick={this.props.onToggle}><Icon name="bars" /></div>

                    <Breadcrumb
                        size="tiny"
                        icon="right angle"
                        sections={[
                            { key: 0, content: 'Профиль', to: '/profile', as: (NavLink), activeClassName: '' },
                            { key: 1, content: 'Настройка почты', link: false },
                        ]} />
                </Grid.Column>

                <Divider />

                <Grid.Column>
                    <Form>
                        <Form.Input
                            label="Email-адрес"
                            name="email"
                            type="text"
                            value={this.state.elements.email.value}
                            placeholder="Email-адрес"
                            autoComplete="off"
                            error={elements.email.error ? { content: elements.email.error } : null}
                            onChange={event => this.inputChangeHandler(event, "email")} />

                        <Grid columns={1}>
                            <Grid.Column textAlign="left">
                                <Form.Button
                                    primary
                                    size="small"
                                    type="submit"
                                    loading={this.state.loading}
                                    disabled={!this.state.valid || this.state.loading}
                                    onClick={this.submitFormHandler}
                                >
                                    Сохранить
                                </Form.Button>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Grid.Column>
            </Grid.Row>
        )
    }

    /**
     * Проверка верности заполнения формы
     */
    isValid = elements => {
        return elements.email.value !== auth().email &&
            emailValidator(elements.email.value)
    }
}

export default ProfileEmail
