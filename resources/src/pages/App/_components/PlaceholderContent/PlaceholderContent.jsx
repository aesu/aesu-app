/**
 * Блок отображения процесса загрузки
 */
import React from 'react'
import { Grid, Placeholder } from 'semantic-ui-react'

const PlaceholderContent = props => {
    let placeholder = (
            <Grid.Column>
                <Placeholder fluid={ props.fluid } >
                    <Placeholder.Header image>
                        <Placeholder.Line />
                        <Placeholder.Line />
                    </Placeholder.Header>
                    <Placeholder.Paragraph>
                        <Placeholder.Line />
                        <Placeholder.Line />
                        <Placeholder.Line />
                        <Placeholder.Line />
                    </Placeholder.Paragraph>
                </Placeholder>
            </Grid.Column>
    )

    if (props.grid) placeholder = (
        <Grid centered columns={ 1 }>
            { placeholder }
        </Grid>
    )

    return ( placeholder )
}

export default PlaceholderContent
