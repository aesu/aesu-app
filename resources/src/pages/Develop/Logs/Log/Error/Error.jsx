/**
 * Ошибка и исключение
 */
import React, { useState } from "react"
import { Accordion, Button, Grid, Label, Segment } from "semantic-ui-react"
import dayjs from "dayjs"

const Error = (props) => {
    const [open, setOpen] = useState(false)

    const { id, url, file, line, message, trace, count } = props.error

    const createdAt = dayjs(props.error.created_at).format(
        "DD.MM.YYYY HH:mm:ss",
    )
    const updatedAt = dayjs(props.error.updated_at).format(
        "DD.MM.YYYY HH:mm:ss",
    )

    return (
        <Grid.Column
            style={{ padding: "0.25rem 0.5rem", overflowWrap: "break-word" }}
        >
            <Accordion
                styled
                fluid
                style={{ borderRadius: "0" }}
            >
                <Accordion.Title
                    active={open}
                    onClick={() => setOpen(!open)}
                >
                    <Grid>
                        <Grid.Row style={{ alignItems: "center" }}>
                            <Grid.Column style={{ flexGrow: "1" }}>
                                <span>#{id} {message || "-"}</span>
                            </Grid.Column>
                            <Grid.Column
                                style={{ width: "65px", textAlign: "right" }}
                            >
                                <Label basic circular>{count}</Label>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Accordion.Title>

                <Accordion.Content
                    active={open}
                    style={{ padding: "8px 1rem" }}
                >
                    {url && (
                        <p>
                            <b>URL:</b>
                            {url}
                        </p>
                    )}
                    {file && (
                        <p>
                            <b>Path:</b>
                            {`${file}:${line}`}
                        </p>
                    )}

                    <b>Stacktrace:</b>
                    <Segment secondary style={{ borderRadius: "0" }}>
                        {trace.map((elem, index) => (
                            <div key={index}>{elem}</div>
                        ))}
                    </Segment>

                    <p>
                        <b>Created:</b>
                        {createdAt}
                    </p>
                    <p>
                        <b>Updated:</b>
                        {updatedAt}
                    </p>

                    <Grid>
                        <Grid.Column>
                            <div style={{ float: "right" }}>
                                <Button
                                    circular
                                    size="mini"
                                    icon="trash"
                                    onClick={props.delete}
                                />
                            </div>
                        </Grid.Column>
                    </Grid>
                </Accordion.Content>
            </Accordion>
        </Grid.Column>
    )
}

export default Error
