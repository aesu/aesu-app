/**
 * Отображение списка ошибок и исключений
 */
import React from "react"
import { Button, Grid } from "semantic-ui-react"

import ErrorInfo from "./Error/Error.jsx"

const Log = (props) => {
    const { data } = { ...props.error }

    const content = data.length === 0
        ? (
            <Grid.Column
                width={16}
                style={{ textAlign: "center", paddingTop: "8px" }}
            >
                <i>Ошибок нет...</i>
            </Grid.Column>
        )
        : (
            <>
                {data.map((elem, index) => (
                    <ErrorInfo
                        key={elem.id}
                        index={index}
                        error={elem}
                        delete={(_) => props.onDelete(index)}
                    />
                ))}

                <Grid.Column textAlign="right" style={{ padding: "0" }}>
                    <Button
                        basic
                        compact
                        size="mini"
                        style={{ boxShadow: "none", margin: "0" }}
                        onClick={props.onDeleteAll}
                    >
                        удалить все
                    </Button>
                </Grid.Column>
            </>
        )

    return content
}

export default Log
