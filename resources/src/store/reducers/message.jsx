import * as ActionTypes from '../actions/message'

const init = {
    format: null,
    header: null,
    message: null,
}

const message = (state = init, action) => {
    switch (action.type) {
        case ActionTypes.MESSAGE_BOX:
            return {
                ...state,
                format: action.data.format,
                header: action.data.header,
                message: action.data.message || null,
            }
        default: break
    }

    return state
}

export default message
