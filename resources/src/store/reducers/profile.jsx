import * as ActionTypes from '../actions/profile'

const init = {
    statuses: {
        data: [],
        loaded: false,
    },
    branches: {
        data: [],
        loaded: false,
    },
    baskets: {
        data: [],
        loaded: false,
    },
}

const profile = (state = init, action) => {
    switch (action.type) {
        case ActionTypes.PROFILE_STATUS:
            return {
                ...state,
                statuses: action.data
            }
        case ActionTypes.PROFILE_BRANCHES:
            return {
                ...state,
                branches: action.data
            }
        case ActionTypes.PROFILE_BASKET:
            return {
                ...state,
                baskets: action.data
            }
        default: break
    }

    return state
}

export default profile
