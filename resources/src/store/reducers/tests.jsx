import * as ActionTypes from '../actions/tests'

const init = {
    tests: {
        data: [],
        loaded: false,
    },
}

const tests = (state = init, action) => {
    switch (action.type) {
        case ActionTypes.TESTS_TO_PASS:
            return {
                ...state,
                tests: action.data,
            }
        default: break;
    }

    return state
}

export default tests
