/**
 * Основное хранилище приложения
 */
import { createStore, combineReducers } from 'redux'

import * as MessageTypes from './actions/message.jsx'
import * as TestsTypes from './actions/tests.jsx'
import * as ProfileTypes from './actions/profile.jsx'

export {
    MessageTypes,
    TestsTypes,
    ProfileTypes,
}

import message from './reducers/message.jsx'
import tests from './reducers/tests.jsx'
import profile from './reducers/profile.jsx'

let combined = combineReducers({
    message: message,
    tests: tests,
    profile: profile,
})

export default createStore(combined)
