<tr>
    <td class="four wide">{{ $branch->name }}</td>
    <td class="four wide">{{ $branch->speciality->code }}</td>
    <td class="seven wide">{{ $branch->users->count() }}</td>
    <td class="right aligned one wide">
       
		@canany(['admin', 'editor'])
			<a class="control" href="{{ route('editor.branch.edit', [$branch->id]) }}" title="Настройки группы">
				<i class="cog icon"></i>
			</a>
		@endcanany
   	
    </td>
</tr>
