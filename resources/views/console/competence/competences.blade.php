@extends('layouts.main', ['title' => config('app.name').' - Компетенции'])

@section('content')

<!-- [DELETE] -->

<div class="ui stackable grid">
    @include('console.parts.menu')
    
    <div class="column" style="flex-grow:1;">
        
		@canany(['admin', 'editor'])
			<div class="ui one column grid">
				<div class="column" style="padding-bottom:0 !important;">

					{{-- КНОПКА СОЗДАТЬ --}}
					<div class="ui grid">
						<div class="one column right aligned row">
							<div class="column">
								<a id="create" class="ui positive tiny button">Создать</a>
							</div>
						</div>
					</div>

				</div>
			</div>
		@endcanany
        
        <div class="ui one column grid">
            <div id="models" class="column" route-next="{{ route('console.competence.next', ['sort' => Request::input('sort')]) }}">
                <table class="ui very basic unstackable table">
                   	<thead>
                   		<tr>
                   			<th class="fifteen wide">Наименование</th>
                   			<th class="one wide"></th>
                   		</tr>
                   	</thead>
                    <tbody id="models-list" page-count="1"></tbody>
                </table>
            </div>
            <div class="row centered">
                <button id="models-next" class="ui button basic small" type="button">Загрузить еще</button>
            </div>
        </div>
        
    </div>
</div>

{{-- МОДАЛЬНОЕ ОКНО --}}
<div id="modal" class="ui modal">
    <div class="header"></div>
    <div class="content">

        <form id="form" class="ui form" method="post">
            @csrf

            <div class="field">
                <label>Сокращение</label>
                <div class="ui fluid input">
                    <input name="shortname" placeholder="Сокращение" autocomplete="off"></textarea>
                </div>

                <div class="ui basic red pointing prompt label transition" style="display:none;"></div>
            </div>

            <div class="field">
                <label>Наименование компетенции</label>
                <div class="ui fluid input">
                    <textarea name="name" style="height: 110px;" placeholder="Наименование компетенции" autocomplete="off"></textarea>
                </div>

                <div class="ui basic red pointing prompt label transition" style="display:none;"></div>
            </div>
        </form>

    </div>
    <div class="actions">
        <div id="btn-submit" class="ui primary button" style="float:left!important;">Создать</div>
        <div id="btn-remove" class="ui negative button">Удалить</div>
        <div class="ui cancel button">Отмена</div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    {{-- ПОЛУЧИТЬ СЛЕДУЮЩИЕ МОДЕЛИ [get_models] --}}
    const gm = function(){
        let btn = $(this);
		let models = $("#models");
        let page = parseInt($("#models-list").attr("page-count"));
		
        btn.addClass("loading");
        
        $.ajax({
            method: "POST",
            url: models.attr("route-next"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
            dataType: "json",
            data: { page: page },
			complete: function(){
                $("#models-list").attr("page-count", ++page);
                btn.removeClass("loading").closest('.row.centered').show();
			},
            success: function(data){
				let list = models.find("#models-list");
				
                data.views.forEach(function(val){
                    let elem = $(val);
                    elem.find("a").click(be);
                    list.append(elem);
                });

                if (data.last){
                    btn.closest('.row.centered').remove(); 
                }
            },
			error: (jq, text, err) => de(jq),
        });
    };
    {{-- РЕДАКТИРОВАНИЕ [button_edit] --}}
    const be = function(){
        let row = $(this).closest("tr");
        
        $("#modal").modal("show dimmer").parent().addClass("inverted");
        
        $.ajax({
            method: "POST",
            url: "{{ route('editor.competence.edit') }}/" + row.attr("model-id"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
            dataType: "json",
            success: function(data){
                $("#modal").find(".header").html("Изменение компетенции").siblings(".actions")
                .find("#btn-submit").html("Сохранить").click(ev => bu(ev, row.attr("model-id")))
                .siblings("#btn-remove").click(() => bd(row.attr("model-id"))).show();
                
                $("#modal").find("input[name=shortname]").val(data.competence.shortname);
                $("#modal").find("textarea").val(data.competence.name);
                
                $("#modal").modal("show");
            },
			error: (jq, text, err) => {
                de(jq);
                $("#modal").modal("hide dimmer");
            },
        });
    };
    {{-- СОЗДАНИЕ [button_create] --}}
    const bc = function(){
        let btn = $(this);
        let form = $("#modal #form");
        
        btn.addClass("loading");
        
        $.ajax({
            method: "POST",
            url: "{{ route('editor.competence.create') }}",
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
            data: form.serialize(),
            dataType: "json",
			complete: function(){
                btn.removeClass("loading");
			},
            success: function(data){
                $("#modal").modal("hide");
                window.location.href = "{{ route('console.competence.main') }}";
            },
			error: (jq, text, err) => {
                if ("errors" in jq.responseJSON){
                    let errors = jq.responseJSON.errors;
                    
                    if ("name" in errors){
                        form.find("textarea")
                        .closest(".field").addClass("error")
                        .find(".ui.basic.label").html(errors.name).slideDown(50);
                    }
                }
            },
        });
    };
    {{-- ОБНОВЛЕНИЕ [button_update] --}}
    const bu = function(ev, id){
        let btn = $(ev.target);
        let form = $("#modal #form");
        
        btn.addClass("loading");
        
        $.ajax({
            method: "POST",
            url: "{{ route('editor.competence.update') }}/" + id,
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
            data: form.serialize(),
            dataType: "json",
			complete: function(){
                btn.removeClass("loading");
			},
            success: function(data){
                $("#modal").modal("hide");
                window.location.href = "{{ route('console.competence.main') }}";
            },
			error: (jq, text, err) => {
                if ("errors" in jq.responseJSON){
                    let errors = jq.responseJSON.errors;
                    
                    if ("name" in errors){
                        form.find("textarea")
                        .closest(".field").addClass("error")
                        .find(".ui.basic.label").html(errors.name).slideDown(50);
                    }
                } else {
                    de(jq);
                    $("#modal").modal("hide");
                }
            },
        });
    };
    {{-- УДАЛЕНИЕ [button_delete] --}}
    const bd = function(id){
        let val = confirm("Вы уверены, что хотите удалить компетенцию? " + id);
		
        if (val) $("#modal #form").attr("action", "{{ route('editor.competence.delete') }}/" + id).off("submit").submit();
        
        return false;
    };
    
    $("#modal").modal({ inverted: true, transition: "scale", duration: 200, onHidden: () => {
        $("#modal input[name=shortname]").val("");
        $("#modal textarea").val("").css("height", "110px").click();
        $("#modal .button").off();
    }});
    
    $("#modal textarea").click(function(){ $(this).closest(".field").removeClass("error").children(".ui.basic.label").slideUp(50); });
    
    $("#models-next").click(gm).click();
    
    $("#create").click(function(){
        $("#modal").find(".header").html("Создание компетенции").siblings(".actions")
        .find("#btn-submit").html("Создать").click(bc)
        .siblings("#btn-remove").hide();
        
        $("#modal").modal("show");
    });
</script>

@endsection
