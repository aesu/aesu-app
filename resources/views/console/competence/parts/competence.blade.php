<tr model-id="{{ $competence->id }}">
    <td class="fifteen wide">{{ $competence->name }}</td>
    <td class="right aligned one wide">
       
		@canany(['admin', 'editor'])
			<a class="control" title="Настройки компетенции">
				<i class="cog icon"></i>
			</a>
		@endcanany
   
    </td>
</tr>
