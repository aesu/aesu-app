@extends('layouts.main', ['title' => config('app.name').' - Вопросы'])

@section('content')

<!-- [DELETE] -->

<div class="ui stackable grid">
    @include('console.parts.menu')
    
    <div class="column" style="flex-grow:1;">
        
        <div class="ui one column grid">
            <div class="column" style="padding-bottom:0 !important;">
                
                {{-- КНОПКА СОЗДАТЬ --}}
                @canany(['admin', 'editor'])
					<div class="ui grid">
						<div class="one column right aligned row">
							<div class="column">
								<a class="ui positive tiny button" href="{{ route('editor.quest.new') }}">Создать</a>
							</div>
						</div>
					</div>
                @endcanany
                
                {{-- ФОРМА ПОИСКА --}}
                <div class="ui form">
                    
                    {{-- СОРТИРОВКА --}}
                    <div class="field">
                        <div class="ui dropdown" tabindex="-1">
                            <i class="sort amount down icon"></i>
                            <span>Сортировать</span>
                            <div class="menu transition hidden" tabindex="-1">
                                <a class="item{{ Request::input('sort') == null ? ' active selected' : '' }}" href="{{ route('console.quest.main') }}">По алфавиту (А - Я)</a>
                                <a class="item{{ Request::input('sort') == 'za' ? ' active selected' : '' }}" href="{{ route('console.quest.main', ['sort' => 'za']) }}">По алфавиту (Я - А)</a>
                            </div>
                        </div>
                    </div>
                    
                    {{-- ПОИСКОВА СТРОКА --}}
                    <div class="field">            
                        <div class="column">
                            <div class="ui fluid icon input">
                                <input id="search-text" type="text" placeholder="Вопрос">
                                <i id="search-icon" class="search link icon"></i>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <div class="ui one column grid">
            <div id="models" class="column" route-next="{{ route('console.quest.next', ['sort' => Request::input('sort')]) }}" route-search="{{ route('console.quest.search', ['sort' => Request::input('sort')]) }}">
                <table class="ui very basic unstackable table">
                   	<thead>
                   		<tr>
                   			<th class="fifteen wide">Формулировка</th>
                   			<th class="one wide"></th>
                   		</tr>
                   	</thead>
                    <tbody id="models-list" page-count="1"></tbody>
                </table>
            </div>
            <div class="row centered">
                <button id="models-next" class="ui button basic small" type="button">Загрузить еще</button>
            </div>
        </div>
        
    </div>
</div>

@endsection

@section('script')

@include('console.parts.search')

@endsection
