@extends('layouts.main', ['title' => config('app.name').' - Заявленная статья  - '.$article->name])

@section('content')

<div class="ui centered grid">

    <div class="sixteen wide mobile tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.conference.main') }}">Конференция</a>
        </div>

        <div class="ui divider"></div>

        {{-- ОСНОВНЫЕ ПАРАМЕТРЫ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>


            <div class="content active">

                <div class="main-holder">

                    @include('editor.conference.article.parts.info')

                </div>

            </div>
        </div>

        <div class="ui divider"></div>

        {{-- СПИСОК УЧАСТНИКОВ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Авторы</b>
            </div>

            <div class="content active">
                <br/>
                <div class="link-holder">

                    @include('editor.conference.article.parts.tablemember')


                </div>
            </div>
        </div>

        <div class="ui divider"></div>

        {{-- ПРИЛОЖЕННЫЕ ФАЙЛЫ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Приложенные документы</b>
            </div>

            <div class="content active">
                <br/>
                <div class="link-holder">

                    @include('editor.conference.article.parts.tabledocument')


                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    $('#delete-item').click(function() {

        const isDelete = confirm("Вы уверены, что хотите удалить статью безвозвратно?");

        if (!isDelete) {
            return
        }

        $.ajax({
            type: 'DELETE',
            url: `/api/conf/article/{{ $article->id }}`,
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
        })
            .done(data => {
                window.location.href = '/console/conference'
            })
            .fail((jqXHR, textStatus, errorThrown) => {
                console.log('$ jqXHR', jqXHR)
                console.log('$ textStatus', textStatus)
                console.log('$ errorThrown', errorThrown)
            })
            .always(() => {

            })
    })

    $('#status').dropdown('set selected', {{ $article->status->id }})
        .dropdown({
            onChange: function (index) {
                $(this).addClass('loading')

                $.ajax({
                        type: 'PUT',
                        url: `/api/conf/article/{{ $article->id }}/status`,
                        data: { statusId: index },
                        headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
                    })
                    .done(data => {
                        
                    })
                    .fail((jqXHR, textStatus, errorThrown) => {
                        console.log('$ jqXHR', jqXHR)
                        console.log('$ textStatus', textStatus)
                        console.log('$ errorThrown', errorThrown)
                    })
                    .always(() => {
                        $(this).removeClass('loading')
                    })
            }
    })

</script>


@endsection
