<div class="ui segment basic left aligned">
    <div class="ui one column grid">

        <div class="column">
            <table class="ui very basic unstackable table">
                <tbody>

                    <tr>
                        <td class="four wide"><b>Заглавие</b></td>
                        <td class="twelve wide">{{ $article->name }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Уровень обучения</b></td>
                        <td class="twelve wide">{{ $article->section->name }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Дата подачи</b></td>
                        <td class="twelve wide">{{ date('d.m.Y', $article->created_at->timestamp) }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Статус</b></td>
                        <!-- <td class="twelve wide">{{ $article->status->name }}</td> -->
                        <td class="twelve wide">
                            <div id="status" class="ui selection dropdown">
                                <input type="hidden" name="gender">
                                <i class="dropdown icon"></i>
                                <div class="default text">Статус</div>

                                <div class="menu">
                                    @foreach ($statuses as $status)
                                        <div class="item" data-value="{{ $status->id }}">{{ $status->name }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </td>

                    </tr>

                </tbody>
            </table>
        </div>

        @canany(['admin', 'delete'])
            <div class="column right aligned">
                <a id="delete-item" class="control" title="Удалить">
                    <i class="trash link icon"></i>
                </a>
            </div>
        @endcanany

    </div>
</div>
