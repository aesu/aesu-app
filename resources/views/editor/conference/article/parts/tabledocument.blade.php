<form class="ui form" method="post">
    <div class="field">

        <table class="ui very basic unstackable table">
            <thead>
                <tr>
                    <th class="one wide center aligned" style="padding-left:5px;">#</th>
                    <th class="fourteen wide">Документ</th>
                    <th class="center aligned one wide"></th>
                </tr>
            </thead>

            <tbody>

                @if ($article->documents->count() == 0)
                    <tr>
                        <td class="center aligned">-</td>
                        <td>-</td>
                        <td class="center aligned"></td>
                    </tr>
                @endif

                @foreach ($article->documents as $key => $document)
                    <tr>
                        <td id="count" class="center aligned">{{ $key + 1 }}</td>

                        <td>{{ $document->name.'.'.$document->extension }}</td>

                        <td class="center aligned">

                            <div class="ui right floated">
                                <a class="control" target="_blank" href="/document/conf?{{ $document->id }}_{{ $document->last_modified->timestamp }}" title="Скачать документ">
                                    <i class="save icon"></i>
                                </a>
                            </div>

                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</form>
