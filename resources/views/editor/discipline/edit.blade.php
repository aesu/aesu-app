@extends('layouts.main', ['title' => config('app.name').' - Дисциплина - '.$disc->name])

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('css/editor/simditor.css') }}" />

@endsection

@section('content')

<div class="ui centered grid">
    
    <div class="sixteen wide mobile tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.disc.main') }}">Дисциплины</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $disc->id }}</div>
        </div>
        
        <div class="ui divider"></div>
        
        {{-- ОСНОВНЫЕ ПАРАМЕТРЫ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>


            <div class="content active">

                <div class="main-holder" route-pairs="{{ route('editor.disc.pairs', [$disc->id]) }}" route-update="{{ route('editor.disc.update', ['id' => $disc->id]) }}">

                    @include('editor.discipline.parts.info')

                </div>

            </div>

        </div>

        <div class="ui divider"></div>

        {{-- ФАКУЛЬТАТИВНЫЕ ГРУППЫ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Факультативные группы</b>
            </div>

            <div class="content">

                <br/>
                <div class="link-holder" route-pairs="{{ route('editor.disc.branchpairs', [$disc->id]) }}" route-search="{{ route('editor.disc.branchsearch') }}" route-update="{{ route('editor.disc.branchupdate', [$disc->id]) }}" model="branches">

                    @include('editor.discipline.parts.tablebranch')

                </div>

            </div>
        </div>

        <div class="ui divider"></div>

        {{-- АНКЕТЫ И ТЕСТЫ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Анкеты и тесты</b>
            </div>

            <div class="content">

                <br/>
                <div>

                    @include('editor.discipline.parts.tabletest')

                </div>

            </div>
        </div>

        <div class="ui divider"></div>

        {{-- СЛОВАРЬ КОМПЕТЕНЦИЙ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Компетенции</b>
            </div>

            <div class="content">

                <br/>
                <div class="link-holder" route-update="{{ route('editor.disc.compupdate', [$disc->id]) }}">

                    @include('editor.discipline.parts.tablecompetence')

                </div>

            </div>
        </div>

        {{-- СЛОВАРЬ ДИДАКТИЧЕСКИХ ЕДИНИЦ --}}
        {{--<div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Дидактические единицы</b>
            </div>

            <div class="content active1">

                <br/>
                <div id="link-holder">

                    @@include('editor.discipline.parts.tabledidactic')

                </div>

            </div>
        </div>--}}

        {{-- СЛОВАРЬ ЗАДАЧ --}}
        {{--<div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Задачи</b>
            </div>

            <div class="content">

                <br/>
                <div id="link-holder">

                    @include('editor.discipline.parts.tabletask')

                </div>

            </div>
        </div>--}}

        {{-- СЛОВАРЬ МЕТОДОВ --}}
        {{--<div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Методы</b>
            </div>

            <div class="content">

                <br/>
                <div id="link-holder">

                    @include('editor.discipline.parts.tablemethod')

                </div>

            </div>
        </div>--}}

    </div>
</div>

@endsection

@section('script')

@include('editor.parts.attach')
@include('editor.discipline.parts.script')

<script type="text/javascript" src="{{ asset('js/editor/module.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/hotkeys.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/simditor.min.js') }}"></script>
<script type="text/javascript">
    {{-- КНОПКА УДАЛИТЬ [button_delete] --}}
    const bd = function(){
        let val = confirm("Вы уверены, что хотите удалить дисциплину?");

        if (val) $(".main-holder form").attr("action", "{{ route('editor.disc.delete', [$disc->id]) }}").off("submit").submit();
    };
    {{-- ИНИЦИАЛИЗАЦИЯ РЕДАКТОРА ОПИСАНИЯ [initialize_editor] --}}
    const ie = function(){
        let editor = new Simditor({
            toolbar: [
                "bold", "italic", "underline", "strikethrough", "color", "|",
                "ol", "ul", "|",
                "indent", "outdent", "|",
                 "link", "hr"
            ],
            textarea: $("#portrait"),
            placeholder: "Вступительное описание теста"
        });
    };
    {{-- УДАЧНОЕ ОБНОВЛЕНИЕ МОДЕЛИ [on_success] --}}
	const osc = function(holder, data, edit){
		holder.empty().append(data.view);
		holder.find("#btn-pairs").click(function(){ gmf(this) });

        console.log(data);

		if (edit){
			ie(); // [initialize_editor]
			holder.find("form").submit(bms);
			holder.find("#btn-delete").click(bd);
			holder.find("#status").dropdown("set selected", `${data.disc.status}`);
		}
	};
    
    console.log($(".link-holder form").find("#btn-pairs"))
    
    $(".main-holder #btn-pairs").click(function(){ gmf(this) });
    $(".link-holder form").find("#btn-pairs").click(gaf);
    $(".link-holder form").find("#btn-save").click(bas);
</script>


@endsection
