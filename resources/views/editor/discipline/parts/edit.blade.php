<div class="ui segment basic left aligned">
    
    @php
        $toEdit = Request::route()->getName() == 'editor.disc.pairs';
    @endphp
    
    {{-- ФОРМА ТОЛЬКО СОЗДАЕТ ДИСЦИПЛИНУ, ОБНОВЛЕНИЕ ПРОИСХОДИТ АСИНХРОННО --}}
    <form class="ui form" action="{{ $toEdit ? '' : route('editor.disc.create') }}" method="POST">
        @csrf

        {{-- НАИМЕНОВЕНИЕ --}}
        <div class="field{{ $errors->has('name') ? ' error' : ''}}">
            <label>Наименование</label>
            <input name="name" type="text" value="{{ $disc->name ?? old('name') }}" placeholder="Наименование" autocomplete="off">
            
            @if ($errors->has('name'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('name') }}
                </div>
            @endif
            
        </div>

        {{-- СОКРАЩЕНИЕ --}}
        <div class="field{{ $errors->has('shortname') ? ' error' : ''}}">
            <label>Сокращение</label>
            <input name="shortname" type="text" value="{{ $disc->shortname ?? old('shortname') }}" placeholder="Сокращение" autocomplete="off">
            
            @if ($errors->has('shortname'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('shortname') }}
                </div>
            @endif
            
        </div>

        {{-- СТАТУС --}}
        <div class="field{{ $errors->has('status') ? ' error' : ''}}">
            <label>Статус</label>
            <div id="status" class="ui fluid dropdown selection" tabindex="0">
                <select name="status">
                    <option value="">Статус</option>
                    
                    @foreach($statuses as $status)
                        <option value="{{ $status->id }}">{{ $status->name }}</option>
                    @endforeach
                    
                </select>
                <i class="dropdown icon"></i>
                <div class="default text">Статус</div>
                <div class="menu transition hidden" tabindex="-1">
                   
                    @foreach($statuses as $status)
                        <div class="item" data-value="{{ $status->id }}">{{ $status->name  }}</div>
                    @endforeach
                    
                </div>
            </div>
            
            @if ($errors->has('status'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('status') }}
                </div>
            @endif
            
        </div>
        
        {{-- ОПИСАНИЕ К ТЕСТУ --}}
        <div class="field{{ $errors->has('portrait') ? ' error' : ''}}">
            <label>Описание</label>
            <textarea id="portrait" name="portrait">{!! $disc->portrait ?? '' !!}</textarea>
            
            @if ($errors->has('portrait'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('portrait') }}
                </div>
            @endif
			
        </div>

        <div class="ui two column grid">

            <div class="left aligned column">
                <div class="column">
                    
                    <button id="btn-save" class="ui primary button" type="submit">{{ $toEdit ? 'Сохранить' : 'Создать' }}</button>
                    
                </div>
            </div>

            <div class="right aligned column">
                <div class="row">

                    @if ($toEdit)
                        <button id="btn-delete" class="ui negative button" type="button">Удалить</button>
                        
                        <button id="btn-pairs" class="ui button" type="button">Отмена</button>
                    @else
                        <a class="ui button" href="{{ route('console.disc.main') }}">Отмена</a>
                    @endif

                </div>
            </div>

        </div>

    </form>
    
</div>
