<form class="ui form" method="post">
    <div class="field">
        
        <table class="ui very basic unstackable table">
            <thead>
               	<tr>
                    <th class="one wide center aligned" style="padding-left:5px;">#</th>
                    <th class="fourteen wide">Наименование</th>
                    <th class="center aligned one wide"></th>
               	</tr>
            </thead>
            
            <tbody>
                
                @if ($didactics->count() == 0)
                    <tr>
                        <td class="center aligned">-</td>
                        <td>-</td>
                        <td></td>
                    </tr>
                @endif
                
                @for ($q = 0; $q < $didactics->count(); $q++)
                    <tr>
                        <td id="count" class="center aligned">{{ $q + 1 }}</td>
						
                        <td>{{ $didactics[$q]->name }}</td>
                        
                        <td class="center aligned">

                            @if ($edit)
                                <button id="btn-remove" class="ui icon basic negative mini button" type="button">
                                    <i class="trash icon"></i>
                                </button>
                            @else
                              
                               @canany(['admin'])
									<div class="ui right floated">
										<a class="control" href="#!" title="Просмотр пользователя">
											<i class="cog icon"></i>
										</a>
									</div>
								@endcanany
                           
                            @endif

                        </td>
                    </tr>
                @endfor
                
            </tbody>
        </table>
        
        @if ($edit)
            <div class="ui two column grid">
                <div class="left aligned column">
                    <div class="column">
                        <button id="btn-save" class="ui primary button" type="submit">Сохранить</button>
                        <button id="btn-add" class="ui basic positive button" type="button">
                        	<i class="plus icon"></i>Добавить
                        </button>
                    </div>
                </div>
                <div class="right aligned column">
                    <div class="column">
                        <button id="btn-pairs" class="ui basic button" type="button" current-status="edit">Отмена</button>
                    </div>
                </div>
            </div>
        @else
        
            <div class="ui one column grid">
                <div class="right aligned column">
                    <div class="column">
                        <button id="btn-pairs" class="ui positive button" type="button" current-status="info">Изменить</button> 
                    </div>
                </div>
            </div>
        
        @endif
        
    </div>
</form>
