{{-- ШАБЛОН ДЛЯ РЕДАКТИРОВАНИЯ --}}
<script type="text/javascript">
	{{-- СОХРАНЕНИЕ ИЗМЕНЕНИЙ ДЛЯ ФОРМЫ РЕДАКТИРОВАНИЯ [button_main_save] --}}
    const bms = function(ev){
        let holder = $(ev.target).closest("div.main-holder");
        let btn = holder.find("#btn-save");
        ev.preventDefault();
        
        btn.addClass("loading");
        
        $.ajax({
            method: "post",
            url: holder.attr("route-update"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'), },
            data: holder.find("form").serialize(),
            dataType: "json",
            complete: function(){
                btn.removeClass("loading");
            },
            success: function(data){
                holder.empty().append(data.view);
                holder.find("#btn-pairs").click(function(){ gmf(this) });
            },
			error: function(jq, text, err){
                window.scrollTo(0, 0);
                let errors = jq.responseJSON.errors;
                if (errors){
                    for (let msg in errors){
                        $('#message').children("p").text(errors[msg]);
                        break;
                    }
                }
                else {
                    $('#message').children("p").text(jq.responseJSON.message);
                }
                $('#message').closest(".grid").slideDown(50);
            },
        });
    };
    {{-- ПЕРЕХОД МЕЖДУ ПРОСМОТРОМ И РЕДАКТИРОВАНИЕМ [get_main_form] --}}
    const gmf = function(ev){
        let btn = $(ev);
        let holder = btn.closest("div.main-holder");
        
        btn.addClass("loading");
        let edit = holder.find("#btn-save").length == 0;
        
        $.ajax({
            method: "post",
            url: holder.attr("route-pairs"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'), },
            data: { edit: edit },
            dataType: "json",
            complete: function(){
                btn.removeClass("loading");
            },
            success: function(data){
				osc(holder, data, edit);
            },
			error: (jq) => de(jq),
        });
    };
	{{-- СОХРАНИТЬ ПРИВЯЗКИ [button_attachment_save] --}}
    const bas = function(ev){
		let btn = $(ev.target);
		let holder = btn.closest(".link-holder");
        ev.preventDefault();
        
        btn.addClass("loading");
		
        $.ajax({
            method: "post",
            url: holder.attr("route-update"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'), },
            data: holder.find("form").serialize(),
            dataType: "json",
            complete: function(){
                btn.removeClass("loading");
            },
            success: function(data){
                holder.find("#btn-pairs").click();
            },
			error: (jq) => de(jq),
        });
    };
    {{-- ПЕРЕХОД МЕЖДУ ПРОСМОТРОМ И РЕДАКТИРОВАНИЕМ ПРИВЯЗОК [get_attachment_form] --}}
    const gaf = function(){
        let btn = $(this);
        let holder = btn.closest(".link-holder");
        
        btn.addClass("loading");
        let edit = holder.find("#btn-save").length == 0;
        
        $.ajax({
            method: "post",
            url: holder.attr("route-pairs"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'), },
            data: { edit: edit },
            dataType: "json",
            complete: function(){
                btn.removeClass("loading");
            },
            success: function(data){
                holder.empty().append(data.view);
                holder.find("#btn-pairs").click(gaf);
                
                if (edit){
                    ae(holder); // [add_events]
					holder.find("#btn-save").click(bas);
                }               
            },
			error: (jq) => de(jq),
        });
    };
</script>