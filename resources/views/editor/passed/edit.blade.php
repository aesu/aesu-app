@extends('layouts.main', ['title' => config('app.name').' - Пройденный #'.$basket->id])

@section('content')

<div class="ui stackable grid">

    <div class="twelve wide tablet fourteen wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.passed.main') }}">Пройденные</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $basket->id }}</div>
        </div>

        <div class="ui divider"></div>

        <div class="ui card loading square">

            <div class="content">
                <div id="result-dimmer" class="ui active inverted dimmer">
                    <div class="ui active loader"></div>
                </div>

                <div id="result-content" class="description"></div>

            </div>

            @canany(['admin', 'delete'])
                <div class="extra content">
                    <div class="right floated">
                        <a id="delete-item" class="control" title="Удалить">
                            <i class="trash link icon"></i>
                        </a>
                        <form id="delete-form" class="ui form" action="{{ route('editor.passed.delete', [$basket->id]) }}" method="post">@csrf</form>
                    </div>
                </div>
            @endcanany

        </div>

    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">

$.ajax({
        url: `/api/basket/{{ $basket->id }}/result-html`,
    })
    .done(data => {
        $('#result-content').empty()
            .append(data)
    })
    .fail((jqXHR, textStatus, errorThrown) => {
        console.log('$ jqXHR', jqXHR)
        console.log('$ textStatus', textStatus)
        console.log('$ errorThrown', errorThrown)
    })
    .always(() => {
        $('#result-dimmer').hide()
    })

</script>

@canany(['admin', 'delete'])
    <script type="text/javascript">
        {{-- УДАЛИТЬ ПРОЙДЕННЫЙ ТЕСТ --}}
        $("#delete-item").click(function(ev){
            let val = confirm("Вы уверены, что хотите удалить пройденные результаты тестирования?");

            if (val) $("#delete-form").submit();
        });
    </script>
@endcanany

@endsection
