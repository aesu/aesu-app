@extends('layouts.main', ['title' => config('app.name').' - Специальность - '.$spec->name])

@section('content')

<div class="ui centered grid">
    
    <div class="sixteen wide mobile tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.spec.main') }}">Специальности</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $spec->id }}</div>
        </div>
        
        <div class="ui divider"></div>
        
        {{-- ОСНОВНЫЕ ПАРАМЕТРЫ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>

            <div class="content active">

                <div class="main-holder" route-pairs="{{ route('editor.spec.pairs', [$spec->id]) }}" route-update="{{ route('editor.spec.update', [$spec->id]) }}">

                    @include('editor.speciality.parts.info')

                </div>

            </div>
        </div>

        {{-- КОМПЕТЕНТНОСТИ --}}
        {{--
        Использование компетенций связанных со специальностями не производится,
        поэтому эта возможность скрыта (так же таблица 'speclinkcomp' была удалена)
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Компетенции</b>
            </div>

            <div class="content">
                <br/>
                <div class="link-holder" route-update="{{ route('editor.spec.compupdate', [$spec->id]) }}">

                    @include('editor.speciality.parts.tablecompetence')

                </div>
            </div>
        </div>
        --}}

    </div>
</div>

@endsection

@section('script')

@include('editor.parts.attach')

<script type="text/javascript">
    {{-- КНОПКА УДАЛИТЬ [button_delete] --}}
    const bd = function(){
        let val = confirm("Вы уверены, что хотите удалить специальность?");
		
        if (val) $(".main-holder form").attr("action", "{{ route('editor.spec.delete', [$spec->id]) }}").off("submit").submit();
    };
    {{-- УДАЧНОЕ ОБНОВЛЕНИЕ МОДЕЛИ [on_success] --}}
	const osc = function(holder, data, edit){
		holder.empty().append(data.view);
		holder.find("#btn-pairs").click(function(){ gmf(this); });

		if (edit){
			holder.find("form").submit(bms);
			holder.find("#btn-delete").click(bd);
		}
	};
    
    $(".main-holder #btn-pairs").click(function(){ gmf(this) });
    $(".link-holder form").find("#btn-save").click(bas);
</script>


@endsection
