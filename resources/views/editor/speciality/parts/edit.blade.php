<div class="ui segment basic left aligned">
    
    @php
        $toEdit = Request::route()->getName() == 'editor.spec.pairs';
    @endphp
    
    {{-- ФОРМА СОЗДАНИЯ И ОБНОВЛЕНИЯ --}}
    <form id="fm-test" class="ui form" action="{{ $toEdit ? '' : route('editor.spec.create') }}" method="POST">
        @csrf

        {{-- НАИМЕНОВЕНИЕ --}}
        <div class="field{{ $errors->has('name') ? ' error' : ''}}">
            <label>Наименование</label>
            <input name="name" type="text" value="{{ $spec->name ?? old('name') }}" placeholder="Наименование" autocomplete="off">
            
            @if ($errors->has('name'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('name') }}
                </div>
            @endif
            
        </div>
        
        {{-- КОД И СОКРАЩЕНИЕ --}}
        <div class="two fields">
            
            <div class="field{{ $errors->has('code') ? ' error' : ''}}">
                <label>Номер</label>
                <input name="code" type="text" value="{{ $spec->code ?? old('code') }}" placeholder="00.00.00" autocomplete="off">
                
                @if ($errors->has('code'))
                    <div class="ui basic red pointing prompt label transition">
                        {{ $errors->first('code') }}
                    </div>
                @endif
                
            </div>
            
            <div class="field{{ $errors->has('shortname') ? ' error' : ''}}">
                <label>Сокращение</label>
                <input name="shortname" type="text" value="{{ $spec->shortname ?? old('shortname') }}" placeholder="Сокращение" autocomplete="off">
                
                @if ($errors->has('shortname'))
                    <div class="ui basic red pointing prompt label transition">
                        {{ $errors->first('shortname') }}
                    </div>
                @endif
                
            </div>
            
        </div>


        <div class="ui two column grid">

            <div class="left aligned column">
                <div class="column">

                    <button id="btn-save" class="ui primary button" type="submit">{{ $toEdit ? 'Сохранить' : 'Создать' }}</button>
                    
                </div>
            </div>

            <div class="right aligned column">
                <div class="row">

                    @if ($toEdit)
                        <button id="btn-delete" class="ui negative button" type="button">Удалить</button>
                        <button id="btn-pairs" class="ui button" type="button">Отмена</button>
                    @else
                        <a class="ui button" href="{{ route('console.test.main') }}">Отмена</a>
                    @endif

                </div>
            </div>

        </div>

    </form>
    
</div>
