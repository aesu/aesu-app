<div class="ui segment basic left aligned">           
    <table class="ui very basic unstackable table">
        <tbody>

            <tr>
                <td><b>Наименование</b></td>
                <td class="right aligned">
                    <button id="btn-pairs" class="ui icon basic small button">
                        <i class="pencil icon"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {{ $spec->name }}
                </td>
            </tr>

            <tr>
                <td class="eight wide"><b>Номер</b></td>
                <td class="eight wide"><b>Сокращение</b></td>
            </tr>
            <tr>
                <td>{{ $spec->code }}</td>
                <td>{{ $spec->shortname ?? '-' }}</td>
            </tr>

        </tbody>
    </table>
</div>
