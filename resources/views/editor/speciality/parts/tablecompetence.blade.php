<form class="ui form" method="post">
    <div class="field">

        <table class="ui very basic unstackable table">
            <thead>
                <tr>
                    <th class="one wide center aligned"></th>
                    <th class="fifteen wide">Компетенция</th>
                </tr>
            </thead>

            <tbody>

                @if ($competences->count() == 0 && !$edit)
                    <tr>
                        <td class="center aligned">-</td>
                        <td>-</td>
                    </tr>
                @endif

                @foreach ($competences as $competence)
                    <tr>
                        <td class="one wide right aligned">
                            <div class="ui checkbox">
                                <input id="c{{ $competence->id }}" type="checkbox" name="competences[]"
                                    value="{{ $competence->id }}" class="hidden" {{ $has->contains('id', $competence->id) ? ' checked' : '' }}>
                                <label for="c{{ $competence->id }}"></label>
                            </div>
                        </td>

                        <td class="fifteen wide">{{ $competence->name }}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="ui ont column grid">
            <div class="left aligned column">
                <div class="column">
                    <button id="btn-save" class="ui primary button" type="submit">Сохранить</button>
                </div>
            </div>
        </div>

    </div>
</form>