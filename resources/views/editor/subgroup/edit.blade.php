@extends('layouts.main', ['title' => config('app.name').' - Спецгруппа - '.$group->name])

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('css/editor/simditor.css') }}" />

@endsection

@section('content')

<div class="ui centered grid">
    
    <div class="sixteen wide mobile tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.subgroup.main') }}">Спецгруппы</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $group->id }}</div>
        </div>
        
        <div class="ui divider"></div>
        
        {{-- ОСНОВНЫЕ ПАРАМЕТРЫ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>


            <div class="content active">

                <div class="main-holder" route-pairs="{{ route('editor.subgroup.pairs', [$group->id]) }}" route-update="{{ route('editor.subgroup.update', ['id' => $group->id]) }}">

                    @include('editor.subgroup.parts.info')

                </div>

            </div>
        </div>

        <div class="ui divider"></div>

        {{-- ПРИВЯЗКА ПОЛЬЗОВАТЕЛЕЙ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Участники</b>
            </div>

            <div class="content">
                <br/>
                <div class="link-holder" route-pairs="{{ route('editor.subgroup.userpairs', [$group->id]) }}" route-search="{{ route('editor.subgroup.usersearch') }}" route-update="{{ route('editor.subgroup.userupdate', [$group->id]) }}" model="users">

                    @include('editor.subgroup.parts.tableuser')

                </div>
            </div>
        </div>

        <div class="ui divider"></div>

        {{-- ПРИВЯЗКА ТЕСТОВ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Анкеты и Тесты</b>
            </div>

            <div class="content">
                <br/>
                <div class="link-holder" route-pairs="{{ route('editor.subgroup.testpairs', [$group->id]) }}" route-search="{{ route('editor.subgroup.testsearch') }}" route-update="{{ route('editor.subgroup.testupdate', [$group->id]) }}" model="tests">

                    @include('editor.subgroup.parts.tabletest')

                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')

@include('editor.parts.attach')
@include('editor.subgroup.parts.script')

<script type="text/javascript" src="{{ asset('js/editor/module.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/hotkeys.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/simditor.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datapick/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datapick/daterangepicker.js') }}"></script>
<script type="text/javascript">
    {{-- КНОПКА УДАЛИТЬ [button_delete] --}}
    const bd = function(){
        let val = confirm("Вы уверены, что хотите удалить спецгруппу?");
		
        if (val) $(".main-holder form").attr("action", "{{ route('editor.subgroup.delete', [$group->id]) }}").off("submit").submit();
    };
    {{-- ИНИЦИАЛИЗАЦИЯ РЕДАКТОРА ОПИСАНИЯ [initialize_editor] --}}
    const ie = function(){
        let editor = new Simditor({
            toolbar: [
                "bold", "italic", "underline", "strikethrough", "color", "|",
                "ol", "ul", "|",
                "indent", "outdent", "|",
                 "link", "hr"
            ],
            textarea: $("#portrait"),
            placeholder: "Описание спецгуппы"
        });
    };
    {{-- УДАЧНОЕ ОБНОВЛЕНИЕ МОДЕЛИ [on_success] --}}
	const osc = function(holder, data, edit){
		holder.empty().append(data.view);
		holder.find("#btn-pairs").click(function(){ gmf(this) });

		if (edit){
			ie(); // [initialize_editor]
			holder.find("form").submit(bms);
			holder.find("#btn-delete").click(bd);
		}
	};
    
    $(".main-holder #btn-pairs").click(function(){ gmf(this) });
    $(".link-holder form").find("#btn-pairs").click(gaf);
</script>


@endsection
