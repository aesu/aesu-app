<div class="ui segment basic left aligned">
    
    @php
        $toEdit = Request::route()->getName() == 'editor.subgroup.pairs';
    @endphp
    
    {{-- ФОРМА ТОЛЬКО СОЗДАЕТ ТЕСТ, ОБНОВЛЕНИЕ ПРОИСХОДИТ АСИНХРОННО --}}
    <form class="ui form" action="{{ $toEdit ? '' : route('editor.subgroup.create') }}" method="POST">
        @csrf

        {{-- НАИМЕНОВЕНИЕ --}}
        <div class="field{{ $errors->has('name') ? ' error' : ''}}">
            <label>Наименование</label>
            <input name="name" type="text" value="{{ $group->name ?? old('name') }}" placeholder="Наименование" autocomplete="off">
            
            @if ($errors->has('name'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('name') }}
                </div>
            @endif
            
        </div>
        
        {{-- ОПИСАНИЕ К ТЕСТУ --}}
        <div class="field{{ $errors->has('portrait') ? ' error' : ''}}">
            <label>Описание</label>
            <textarea id="portrait" name="portrait">{!! $group->portrait ?? '' !!}</textarea>
            
            @if ($errors->has('portrait'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('portrait') }}
                </div>
            @endif
            
        </div>


        <div class="ui two column grid">

            <div class="left aligned column">
                <div class="column">

                    <button id="btn-save" class="ui primary button" type="submit">{{ $toEdit ? 'Сохранить' : 'Создать' }}</button>
                    
                </div>
            </div>

            <div class="right aligned column">
                <div class="row">

                    @if ($toEdit)
                        <button id="btn-delete" class="ui negative button" type="button">Удалить</button>
                        <button id="btn-pairs" class="ui button" type="button">Отмена</button>
                    @else
                        <a class="ui button" href="{{ route('console.subgroup.main') }}">Отмена</a>
                    @endif

                </div>
            </div>

        </div>

    </form>
    
</div>
