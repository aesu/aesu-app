<div class="ui segment basic left aligned">
    
    @php
        $toEdit = Request::route()->getName() == 'editor.test.pairs';
    @endphp
    
    {{-- ФОРМА ТОЛЬКО СОЗДАЕТ ТЕСТ, ОБНОВЛЕНИЕ ПРОИСХОДИТ АСИНХРОННО --}}
    <form id="fm-test" class="ui form" action="{{ $toEdit ? '' : route('editor.test.create') }}" method="POST">
        @csrf

        {{-- ДИСЦИПЛИНА --}}
        <div class="field{{ $errors->has('discipline') ? ' error' : ''}}">
            <label>Дисциплина</label>
            <div id="discipline" class="ui fluid dropdown selection" tabindex="0">
                <select name="discipline">
                    <option value="">Дисциплина</option>
                    
                    @foreach($discs as $disc)
                        <option value="{{ $disc->id }}">{{ $disc->name }}</option>
                    @endforeach
                    
                    <option value="">-</option>
                </select>
                <i class="dropdown icon"></i>
                <div class="default text">Дисциплина</div>
                <div class="menu transition hidden" tabindex="-1">
                    
                    @foreach($discs as $disc)
                        <div class="item" data-value="{{ $disc->id }}">{{ $disc->name }}</div>
                    @endforeach
                    
                    <div class="item" data-value="">-</div>
                </div>
            </div>
            
            @if ($errors->has('discipline'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('discipline') }}
                </div>
            @endif
            
        </div>

        {{-- НАИМЕНОВЕНИЕ --}}
        <div class="field{{ $errors->has('name') ? ' error' : ''}}">
            <label>Наименование</label>
            <input name="name" type="text" value="{{ $test->name ?? old('name') }}" placeholder="Наименование" autocomplete="off">
            
            @if ($errors->has('name'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('name') }}
                </div>
            @endif
            
        </div>
        
        {{-- ТИП ТЕСТА И ТАЙМЕР --}}
        <div class="two fields">
            
            <div class="field{{ $errors->has('option') ? ' error' : ''}}">
                <label>Тип теста</label>
                <div id="option" class="ui fluid dropdown selection" tabindex="0">
                    <select name="option">
                        <option value="">Тип теста</option>
                        <option value="0">Анкетирование</option>
                        <option value="1">Тестирование</option>
                    </select>
                    <i class="dropdown icon"></i>
                    <div class="default text">Тип теста</div>
                    <div class="menu transition hidden" tabindex="0">
                        <div class="item" data-value="0">Анкетирование</div>
                        <div class="item" data-value="1">Тестирование</div>
                    </div>
                </div>
                
                @if ($errors->has('option'))
                    <div class="ui basic red pointing prompt label transition">
                        {{ $errors->first('option') }}
                    </div>
                @endif
                
            </div>
            
            <div class="field{{ $errors->has('timer') ? ' error' : ''}}">
                <label>Таймер</label>
                <input id="timer" name="timer" type="text" value="{{ $test->timer ?? old('timer') }}" placeholder="Время (секунд)" data-content="Поле можно оставить пустым, если в таймере нет необходимости"  data-position="bottom left" autocomplete="off">
                
                @if ($errors->has('timer'))
                    <div class="ui basic red pointing prompt label transition">
                        {{ $errors->first('timer') }}
                    </div>
                @endif
                
            </div>
            
        </div>
        
        {{-- ОПИСАНИЕ К ТЕСТУ --}}
        <div class="field{{ $errors->has('portrait') ? ' error' : ''}}">
            <label>Описание</label>
            <textarea id="portrait" name="portrait">{!! $test->portrait ?? '' !!}</textarea>
            
            @if ($errors->has('portrait'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('portrait') }}
                </div>
            @endif
            
        </div>


        <div class="ui two column grid">

            <div class="left aligned column">
                <div class="column">

                    <button id="btn-save" class="ui primary button" type="submit">{{ $toEdit ? 'Сохранить' : 'Создать' }}</button>
                    
                </div>
            </div>

            <div class="right aligned column">
                <div class="row">

                    @if ($toEdit)
                        <button id="btn-delete" class="ui negative button" type="button">Удалить</button>
                        <button id="btn-pairs" class="ui button" type="button">Отмена</button>
                    @else
                        <a class="ui button" href="{{ route('console.test.main') }}">Отмена</a>
                    @endif

                </div>
            </div>

        </div>

    </form>
    
</div>
