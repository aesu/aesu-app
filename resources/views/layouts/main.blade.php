<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" version="{{ config('app.version') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }}</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}">

    <link href="{{ asset('css/semantic.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .square{
            width: auto !important;
            border-radius: 0 !important;
        }
        .bordless{
            border-color: transparent !important;
            box-shadow: 0 0 0 0 transparent !important;
        }
        a.control{
            color: rgba(0,0,0,.4) !important;
            margin: 0 0.5px !important;
            padding: 0 0 0 5px !important;
        }
        a.bread{
            color: rgba(0, 0, 0, 0.8) !important;
        }
        a.control:hover{
            color: rgba(0,0,0,.5) !important;
        }
        a.bread:hover{
            text-decoration: underline;
        }
    </style>
    @yield('style')
</head>
<body>
    {{-- ОСНОВНОЙ КОНТЕЙНЕР --}}
    <div>
        @include('layouts.parts.navbar')

        <div class="ui main container" style="padding-bottom:100px!important;">

            {{-- СООБЩЕНИЕ ОБ ОШИБКЕ --}}
            @include('layouts.parts.message')

            @yield('content')
        </div>
    </div>

    {{-- СКРИПТЫ --}}
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/semantic.min.js') }}"></script>
    <script type="text/javascript">
        {{-- ПОКАЗАТЬ ОШИБКУ [display_error] --}}
        const de = function(jq){
            window.scrollTo(0, 0);
            if ("header" in jq.responseJSON) $('#message').children(".ui.header").text(jq.responseJSON.header);
            $('#message').children("p").text(jq.responseJSON.message);
            $('#message').closest(".grid").show(50);
        };
        $('#message').click(function(){
            if (window.getSelection().type != "Range") $(this).closest(".grid").hide();
        });

        $('.ui.dropdown').dropdown();
        $('.ui.accordion').accordion({ "duration": 150 });
    </script>

    @yield('script')
</body>
</html>
