
<!-- [DELETE] -->

<div class="column">
	<div class="ui grid" style="display:{{ $errors->has('message') ? 'block' : 'none' }};">
		<div class="fourteen column row">
			<div class="column"></div>
			<div class="fourteen wide column">
				<div id="message" class="ui negative message">
					<div class="ui header">
						{{ $errors->first('header') }}
					</div>
					<p>
						{{ $errors->first('message') }}
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
