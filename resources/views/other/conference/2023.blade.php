<p class="ap__block ap--center ap--bold ap--big ap--mb-1">
    <span>МИНИСТЕРСТВО НАУКИ И ВЫСШЕГО ОБРАЗОВАНИЯ РФ</span>
    <span>ФГАОУ ВО «СИБИРСКИЙ ФЕДЕРАЛЬНЫЙ УНИВЕРСИТЕТ» (СФУ)</span>
    <span>РОССИЙСКАЯ АССОЦИАЦИЯ ИСКУССТВЕННОГО ИНТЕЛЛЕКТА</span>
</p>

<p class="ap__block ap--center ap--bold ap--big ap--m-1">
    <span>XV ВСЕРОССИЙСКАЯ НАУЧНО-ТЕХНИЧЕСКАЯ</span>
    <span>КОНФЕРЕНЦИЯ С МЕЖДУНАРОДНЫМ УЧАСТИЕМ</span>
    <span>«РОБОТОТЕХНИКА И ИСКУССТВЕННЫЙ ИНТЕЛЛЕКТ»</span>
</p>

<p class="ap__block ap--center ap--bold ap--m-1">
    <span>2 декабря 2023 г.</span>
    <span>г. Железногорск</span>
</p>

<p class="ap--center ap--bold ap--mt-3">
    Уважаемые коллеги!
</p>

<p class="ap--justify ap--indent">
    Приглашаем Вас принять участие в <b>XV Всероссийской научно-технической конференции
        с международным участием «Робототехника и искусственный интеллект» (РИИ-23)</b>,
    посвящённой вопросам развития общей и специальной робототехники, а также
    интеллектуальным системам. Конференция будет проводиться в онлайн-формате
    <b>2 декабря 2023 г.</b> Сибирским федеральным университетом (г. Железногорск Красноярского края).
</p>

<p class="ap--indent ap--bold ap--m-2">
    Научные направления (секции) конференции:
</p>

<ol class="ap__list ap--justify">
    <li>
        <i>Робототехнические системы и комплексы, мехатроника</i>
        (архитектура и реализация полноценных робототехнических комплексов,
        отдельных устройств и вспомогательных механизмов)
    </li>
    <li>
        <i>Специальная робототехника</i>
        (отраслевое применение роботов в космосе, водной и воздушной средах,
        а также при решении специфических социальных, производственных и научных задач)
    </li>
    <li>
        <i>Инженерия знаний и базы знаний</i>
        (подходы к извлечению, формализации и представлению знаний
        в интеллектуальных системах, базы знаний, задачники и
        Data Setы для обучения алгоритмов искусственного интеллекта)
    </li>
    <li>
        <i>Модели и методы анализа данных в интеллектуальных системах</i>
        (нейронные сети и симуляция работы мозга, эволюционные вычисления,
        Data Mining, Text Mining, Graph Mining)
    </li>
    <li>
        <i>Алгоритмы и системы искусственного интеллекта</i>
        (теоретические и прикладные вопросы реализации отдельных методов искусственного интеллекта,
        решателей и полноценных интеллектуальных систем)
    </li>
    <li>
        <i>Управление роботами и интеллектуальная автоматизация</i>
        (искусственный интеллект в задачах управления роботизированными и
        автоматизированными системами, программные системы промышленной автоматизации)
    </li>
    <li>
        <i>Образовательные и социогуманитарные аспекты применения искусственного интеллекта</i>
        (разработка и применение интеллектуальных обучающих систем в учебном процессе,
        образовательная робототехника, социальные и философские вопросы
        применения искусственного интеллекта)
    </li>
</ol>

<p class="ap--justify ap--indent ap--m-0">
    Работа конференции предполагает пленарные и секционные доклады.
    Форма участия – <b>очная</b> (дистанционная) и <b>заочная</b> (только публикация).
    <u>Тексты статей</u> (от 4 полных страниц и до 6) и
    <u>экспертное заключение о возможности публикации в открытой печати</u>
    (кроме последней секции) необходимо разместить на сайте конференции
    https://aesfu.ru/conf <b>с 10 октября по 25 ноября</b> (включительно).
    Материалы, присланные для публикации в сборнике, будут подвержены экспертизе
    и опубликованы только в случае положительной рецензии и наличии всех элементов статьи
    (требования см. в файле-шаблоне).
    Сборнику трудов будет присвоен ISBN, а статьи будут проиндексированы в <b>РИНЦ</b> (elibrary.ru).
</p>

<p class="ap--justify ap--indent ap--m-0">
    Организационный взнос составляет <b>400 рублей</b> за размещение одной статьи.
    Сборник будет издан в электронном виде и разослан авторам по электронной почте
    (документ в pdf-формате). Организационный взнос оплачивается <b>после</b> получения
    положительного решения о включении доклада в сборник.
</p>

<p class="ap--indent ap--bold ap--m-1">
    Последний срок подачи статей – 25 ноября 2023 г. (включительно)!
</p>

<p class="ap--justify ap--indent ap--m-0">
    Всем авторам, чьи доклады будут включены в программу работы конференции,
    будет заблаговременно выслано <u>второе информационное</u> письмо.
</p>

<p class="ap--center ap--bold ap--m-2">
    Программный комитет конференции
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Председатель: Углев Виктор Александрович</b>
    – к.т.н., доцент Института космических и информационных технологий СФУ (г. Железногорск, Россия)
</p>

<p class="ap--center ap--bold ap--m-2">
    Эксперты программного комитета
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Гаврилова Татьяна Альбертовна</b>
    - д.т.н., профессор, заведующий кафедрой информационных технологий
    в менеджменте СПбГУ (г. Санкт-Петербург)
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Гаврилов Андрей Владимирович</b>
    - к.т.н., доцент Новосибирского государственного технического университета
    (г. Новосибирск)
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Горобцов Александр Сергеевич</b>
    – д.т.н., профессор, декан факультета электроники и вычислительной техники ВГТУ
    (г. Волгоград)
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Добронец Борис Станиславович</b>
    – д.ф.-м.н., профессор Института космических и информационных технологий СФУ
    (г. Красноярск)
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Дудоров Евгений Александрович</b>
    – к. т. н., исполнительный директор НПО "Андроидная техника"
    (г. Магнитогорск)
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Котов Артемий Александрович</b>
    – к. филол. н., вед. науч. сотрудник лаборатории нейрокогнитивных технологий НИЦ Курчатовский Института
    (г. Москва)
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Филимонов Вячеслав Аркадьевич</b>
    – д.т.н., профессор филиала Института математики СО РАН (г. Омск)
</p>

<p class="ap--center ap--bold ap--m-2">
    Адрес оргкомитета и контакты
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Почтовый адрес:</b>
    662971, Россия, Красноярский край, г. Железногорск, ул. Кирова, 12а.
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Адрес сайта конференции:</b>
    <span class="user-select">https://aesfu.ru/conf</span>
</p>

<p class="ap--justify ap--indent ap--m-0">
    <b>Адреса электронной почты:</b>
    <span class="user-select">rai-conf@mail.ru</span>
</p>

<p class="ap--center ap--bold ap--m-2">
    Результаты проведения конференции
</p>

<div style="margin-left:36px;">
    <table class="ui unstackable very basic compact table">
        <tbody>
            <tr>
                <td style="width:120px;">Участвовало</td>
                <td>65 человек</td>
            </tr>
            <tr>
                <td style="width:120px;">Подано</td>
                <td>41 статья</td>
            </tr>
            <tr>
                <td style="width:155px;">Электронный сборник</td>
                <td>
                    <a href="/document/local/conference/_docs/2023/RAI-23_print.pdf" title="РИИ, 2 декабря 2023 г."
                        target="_blank">
                        РИИ, 2 декабря 2023 г.
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<p class="ap--center ap--bold ap--m-2">
    Дополнительные материалы
</p>

<ul>
    <li>
        <a href="/document/local/conference/_docs/2023/RAI-23_inf.pdf" target="_blank">Документ основного положения</a>
    </li>
    <li>
        <a href="/document/local/conference/_docs/2023/RAI-23_shablon.doc" target="_blank">Шаблон оформления статьи</a>
    </li>
</ul>