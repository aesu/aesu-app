<html>
<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <title>Uglev Viktor - {{ config('app.name') }}</title>
    <style styles-type="Custom">
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 1.5rem 0 100px;
        }
    </style>
    <style styles-type="WS Word styles">
        /* Style Definitions */
        p.MsoNormal,
        li.MsoNormal,
        div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 0cm;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }
        h2 {
            margin-top: 2.0pt;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 0cm;
            margin-bottom: .0001pt;
            line-height: 107%;
            page-break-after: avoid;
            font-size: 13.0pt;
            font-family: "Calibri Light", sans-serif;
            color: #2E74B5;
            font-weight: normal;
        }
        h3 {
            margin-right: 0cm;
            margin-left: 0cm;
            font-size: 13.5pt;
            font-family: "Times New Roman", serif;
            font-weight: bold;
        }
        h5 {
            margin-top: 2.0pt;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 0cm;
            margin-bottom: .0001pt;
            line-height: 107%;
            page-break-after: avoid;
            font-size: 11.0pt;
            font-family: "Calibri Light", sans-serif;
            color: #2E74B5;
            font-weight: normal;
        }
        a:link,
        span.MsoHyperlink {
            color: blue;
            text-decoration: underline;
        }
        a:visited,
        span.MsoHyperlinkFollowed {
            color: #954F72;
            text-decoration: underline;
        }
        p {
            word-break: break-word;
            margin-right: 0cm;
            margin-left: 0cm;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }
        p.MsoListParagraph,
        li.MsoListParagraph,
        div.MsoListParagraph {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 36.0pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }
        p.MsoListParagraphCxSpFirst,
        li.MsoListParagraphCxSpFirst,
        div.MsoListParagraphCxSpFirst {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }
        p.MsoListParagraphCxSpMiddle,
        li.MsoListParagraphCxSpMiddle,
        div.MsoListParagraphCxSpMiddle {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }
        p.MsoListParagraphCxSpLast,
        li.MsoListParagraphCxSpLast,
        div.MsoListParagraphCxSpLast {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 36.0pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }
        .MsoChpDefault {
            font-family: "Calibri", sans-serif;
        }
        .MsoPapDefault {
            margin-bottom: 8.0pt;
            line-height: 107%;
        }
        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 2.0cm 42.5pt 2.0cm 3.0cm;
        }
        /* List Definitions */
        ol {
            margin-bottom: 0cm;
        }
        ul {
            margin-bottom: 0cm;
        }
    </style>
</head>
<body lang=RU link=blue vlink="#954F72">
    <div class="container">
        <div class=WordSection1>
            <p class=MsoNormal align=center style='text-align:center;text-indent:1.0cm'><b><span lang=EN-US
                        style='font-size:20.0pt;line-height:107%;font-family:"Times New Roman",serif'>Uglev
                        Viktor</span></b></p>
            <p class=MsoNormal style='margin-top:15.75pt;margin-right:0cm;margin-bottom:
7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;background:white'><b><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>Research
                        (Team Member):</span></b></p>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;text-indent:-18.0pt;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>FSRZ-2020-0011
                    grant (2020-2021): Development of principles of creation and technologies of
                    synthesis of materials and products with a multilevel structure based on
                    nanodisperse powders, alloys and compounds of metals, semi-metals and
                    semiconductors, mathematical support and computer science in the field of
                    end-to-end digital technologies</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;text-indent:-18.0pt;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Contract
                    No. 14.596.11.0042 (2017-2019): Creation and launch of a digital platform for knowledge
                    sharing and copyright management (IPUniversity) </span></p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;text-indent:-18.0pt;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Contract
                    No. 02.G25.31.0041 (2012-2014): Creation of high-tech production of modern
                    on-board command and measurement system equipment in standards based on the
                    recommendations of the International Advisory Committee on Space Data Systems
                    (CCSDS) for use on unpressurized spacecraft</span></p>
            <h3 style='margin-top:15.75pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt'>Teaching/Supervising
                    Experience</span></h3>
            <p class=MsoNormal style='margin-bottom:7.9pt;text-indent:1.0cm;line-height:
normal;background:white'><span lang=EN-US style='font-size:14.0pt;font-family:
"Times New Roman",serif'>Lecturer in courses:</span></p>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Artificial
                    intelligence systems</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Data mining</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>System analysis
                    and synthesis</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Data analysis and
                    decision-making</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:36.0pt;text-indent:-18.0pt;line-height:
normal;background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span style='font-size:14.0pt;font-family:"Times New Roman",serif'>Foundation
                    of System Engineering</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Problems of
                    managing complex systems</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:36.0pt;text-indent:-18.0pt;line-height:
normal;background:white'><span lang=EN-US style='font-size:11.5pt;font-family:
"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Foundation
                    of the theory of active systems</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Modeling of
                    systems</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Optimization
                    methods</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Software
                    implementation of mathematical models</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Methodology of
                    scientific activity</span></p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-indent:1.0cm;line-height:normal;
background:white'><span style='font-size:11.5pt;font-family:"Georgia",serif;
color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Modern problems of
                    computer science and high technology</span></p>
            <p class=MsoNormal style='margin-bottom:7.9pt;text-indent:1.0cm;line-height:
normal;background:white'><span lang=EN-US style='font-size:14.0pt;font-family:
"Times New Roman",serif'>Supervising undergraduate dissertations.</span></p>
            <p class=MsoNormal style='margin-bottom:7.9pt;text-indent:1.0cm;line-height:
normal;background:white'><span lang=EN-US style='font-size:14.0pt;font-family:
"Times New Roman",serif'>Member of the Russian Association of Artificial
                    Intelligence</span></p>
            <p class=MsoNormal style='text-indent:1.0cm'><span lang=EN-US style='font-size:
14.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></p>
            <h3 style='margin-top:15.75pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt'>Education</span></h3>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;text-indent:-18.0pt;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Ph.D.
                    Thesis «Models and methods of building systems of training computer testing
                    based on expert systems with elements of fuzzy logic» (Krasnoyarsk, 2009)</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;text-indent:-18.0pt;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Ph.D.
                    Student majoring in «System analysis, control and information processing»
                    (2006-2009)</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;text-indent:-18.0pt;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Master
                    of Engineering and Technology majoring in «System analysis and Control»
                    (2014-2016)</span></p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;text-indent:-18.0pt;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Krasnoyarsk
                    State Technical University majoring in «Applied Informatics» (2001-2006)</span></p>
            <p class=MsoNormal style='margin-bottom:7.9pt;text-indent:1.0cm;line-height:
normal;background:white'><span lang=EN-US style='font-size:14.0pt;font-family:
"Times New Roman",serif'>&nbsp;</span></p>
            <h3 style='margin-top:15.75pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt'>Papers</span></h3>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>2022 (4)</span></h5>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V. <b>Unified Graphic Visualization of Activity (UGVA) Method</b>. Novel &amp;
                    Intelligent Digital Systems: Proceedings of the 2nd International Conference
                    (NiDS 2022). NiDS 2022. <i>LNNS</i>, vol 556. Springer, Cham. pp. 255–265. </span><a
                    href="https://doi.org/10.1007/978-3-031-17601-2_25"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-031-17601-2_25</span></a>
            </p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V.,&nbsp;Gavrilova T. <b>Cross-Cutting Visual Support of Decision Making for
                        Forming Personalized Learning Spaces</b>. Novel &amp; Intelligent Digital
                    Systems: Proceedings of the 2nd International Conference (NiDS 2022). NiDS
                    2022. <i>LNNS</i>, vol 556. Springer, Cham. pp. 3-13. </span><a
                    href="https://doi.org/10.1007/978-3-031-17601-2_1"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-031-17601-2_1</span></a>
            </p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V.A., Zacharyin K.N. <b>Decomposition, depositing and committing of digital
                        footprint of complex composite objects</b>. Computational Science and Its
                    Applications – ICCSA 2022 Workshops. <i>LNCS</i>, vol 13377. Springer, Cham.
                    pp. 242–257. </span><a href="https://doi.org/10.1007/978-3-031-10536-4_17"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-031-10536-4_17</span></a>
            </p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Sychev O., Gavrilova T. <b>Cross-Cutting Support of Making and Explaining
                        Decisions in Intelligent Tutoring Systems Using Cognitive Maps of Knowledge
                        Diagnosis</b>. <i>Intelligent Tutoring Systems. LNCS</i>, vol 13284. Springer,
                    Cham. 2022. pp. 51-64. </span><a href="https://doi.org/10.1007/978-3-031-09680-8_5"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-031-09680-8_5</span></a>
            </p>
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;background:white'><span lang=EN-US style='font-size:14.0pt;font-family:
"Times New Roman",serif'>&nbsp;</span></p>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>2021 (2)</span></h5>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Sychev O. <b>Creating and Visualising Cognitive Maps of Knowledge Diagnosis
                        During the Processing of Learning Digital Footprint</b>. <i>Intelligent
                        Tutoring Systems.</i> <i>LNCS</i>, vol. 12677. Springer, Cham. 2021. pp. 93-98.
                </span><a href="https://doi.org/10.1007/978-3-030-80421-3_11"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-030-80421-3_11</span></a><span
                    lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>.</span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span class=MsoHyperlink><span lang=EN-US style='font-size:11.5pt;font-family:"Georgia",serif;color:#777777;text-decoration:
none'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Sychev O. <b>Concentrating Competency Profile Data Into Cognitive Map of
                        Knowledge Diagnosis</b>. Diagrammatic Representation and Inference. <i>LNCS</i>,
                    vol. 12909. Springer, Cham. 2021. pp. 443-446. </span><a
                    href="https://doi.org/10.1007/978-3-030-86062-2_46"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-030-86062-2_46</span></a>
            </p>
            <p class=MsoListParagraphCxSpLast style='line-height:normal;background:white'><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>202</span><span style='font-size:14.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:windowtext'>0</span><span lang=EN-US style='font-size:14.0pt;line-height:
107%;font-family:"Times New Roman",serif;color:windowtext'> (1)</span></h5>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Zacharyin K., Baryshev R. <b>Cognitive Maps of Knowledge Diagnosis as an
                        Element of a Digital Educational Footprint and a Copyright Object</b>. Software
                    Engineering Perspectives in Intelligent Systems. CoMeSySo 2020. <i>AISC</i>,
                    vol 1295. Springer, Cham. 2020. pp. 349-357. </span><a
                    href="https://doi.org/10.1007/978-3-030-63319-6_31"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-030-63319-6_31</span></a>
            </p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:36.0pt;line-height:normal;background:white'><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>2017 (1)</span></h5>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Cholodilov S., Cholodilova V. <b>Map as a basis for decision-making in the
                        automated learning process.</b> <i>Applied Methods of Statistical Analysis</i>.
                    Nonparametric Methods in Cybernetics and System Analysis: Proceedings of the
                    International Workshop. - Novosibirsk: NSTU publisher, 2017. - pp. 325-334. </span><a
                    href="https://www.scopus.com/record/display.uri?eid=2-s2.0-85044334622&amp;origin=inward&amp;txGid=eb135ecdcf71683e919ec51aa174f818"><span
                        lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://www.scopus.com/record/display.uri?eid=2-s2.0-85044334622&amp;origin=inward&amp;txGid=eb135ecdcf71683e919ec51aa174f818</span></a>
            </p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:36.0pt;text-align:justify;line-height:normal;
background:white'><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>20</span><span style='font-size:14.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:windowtext'>15</span><span lang=EN-US style='font-size:14.0pt;line-height:
107%;font-family:"Times New Roman",serif;color:windowtext'> (</span><span style='font-size:14.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:windowtext'>3</span><span lang=EN-US style='font-size:14.0pt;line-height:
107%;font-family:"Times New Roman",serif;color:windowtext'>)</span></h5>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span class=MsoHyperlink><span lang=EN-US style='font-size:11.5pt;font-family:"Georgia",serif;color:#777777;text-decoration:
none'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Popova O., Dobronets B. <b>The accuracy calculation control of reliability
                        indices for equipment responsible appointment</b>. <i>International Siberian
                        Conference on Control and Communications</i> (SIBCON). – Omsk: OmGTU, 2015. –
                    p. 5-8. </span><a href="https://doi.org/10.1109/SIBCON.2015.7147248"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1109/SIBCON.2015.7147248</span></a>
            </p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Filimonov V., Mishkina N. <b>Hybrid approach to the management by feedback
                        when working with automated educational systems</b>. <i>International Siberian
                        Conference on Control and Communications (SIBCON)</i>. – Omsk: OmGTU, 2015. –
                    p. 1-4. </span><a
                    href="https://www.researchgate.net/profile/Victor-Uglev-2/publication/301344716_Hybrid_approach_to_the_management_by_feedback_when_working_with_automated_educational_systems/links/571385b308ae39beb87ce55d/Hybrid-approach-to-the-management-by-feedback-when-working-with-automated-educational-systems.pdf"><span
                        lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://www.researchgate.net/profile/Victor-Uglev-2/publication/301344716_Hybrid_approach_to_the_management_by_feedback_when_working_with_automated_educational_systems/links/571385b308ae39beb87ce55d/Hybrid-approach-to-the-management-by-feedback-when-working-with-automated-educational-systems.pdf</span></a>
            </p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Rogozhnikova M. <b>Individualized natural language dialogue with the
                        students in the intellectual education system</b>. <i>Interactive Systems and
                        Technologies: the Problem of Human-Computer Interaction</i>. Volume XI -
                    Collection of scientific papers. - Ulyanovsk: UlSTU, 2015. - p. 255-259. –
                    Omsk: OmGTU, 2015. – p. 1-4. </span><a
                    href="https://www.researchgate.net/profile/Victor-Uglev-2/publication/301345473_Individualized_natural_language_dialogue_with_the_students_in_the_intellectual_education_system/links/5713875808aeebe07c0634ac/Individualized-natural-language-dialogue-with-the-students-in-the-intellectual-education-system.pdf"><span
                        lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://www.researchgate.net/profile/Victor-Uglev-2/publication/301345473_Individualized_natural_language_dialogue_with_the_students_in_the_intellectual_education_system/links/5713875808aeebe07c0634ac/Individualized-natural-language-dialogue-with-the-students-in-the-intellectual-education-system.pdf</span></a>
            </p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:1.0cm;text-align:justify;line-height:normal;
background:white'><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>2</span><span lang=EN-US style='font-size:14.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:windowtext'>01</span><span style='font-size:14.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:windowtext'>4</span><span lang=EN-US style='font-size:14.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:windowtext'> (</span><span style='font-size:14.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:windowtext'>3</span><span lang=EN-US style='font-size:14.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:windowtext'>)</span></h5>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Suchinin D. <b>Automated Education: Tendency for Scientific Approaches
                        Convergence</b>. <i>ICASSR2014</i>, - 2014. – pp. 20-23. </span><a
                    href="https://doi.org/10.2991/icassr-14.2014.6"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.2991/icassr-14.2014.6</span></a>
            </p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Ustinov V. <b>The new competencies development level expertise method
                        within Intelligent Automated Educational Systems</b>. Trends in Practical
                    Applications of Heterogeneous Multi-Agent Systems. The PAAMS Collection. <i>AISC</i>,
                    - 2014. – Vol. 293. – pp. 157-164. </span><a
                    href="https://doi.org/10.1007/978-3-319-07476-4_19"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-319-07476-4_19</span></a>
            </p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span class=MsoHyperlink><span lang=EN-US style='font-size:11.5pt;font-family:"Georgia",serif;color:#777777;text-decoration:
none'>-<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><span lang=EN-US
                    style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V. <b>Implementation of Decision-making Methods in Intelligent Automated
                        Educational System Focused on Complete Individualization in Learning</b>. <i>AASRI
                        Procedia</i>, vol. 6, 2014, pp. 66-72. <span class=MsoHyperlink>https://doi.org/10.1016/
                        j.aasri.2014.05.010</span></span></p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:1.0cm;text-align:justify;line-height:normal;
background:white'><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>2011 (</span><span style='font-size:14.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:windowtext'>3</span><span lang=EN-US style='font-size:14.0pt;line-height:
107%;font-family:"Times New Roman",serif;color:windowtext'>)</span></h5>
            <p class=MsoListParagraphCxSpFirst style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Khnykin
                    A., Laletin N., Uglev V. <b>Applying Zheleznogorsk Robotics for Learning
                        Children with Disabilities</b>. <i>LNCS</i>, vol. 6693, 2011, pp. 167-171. </span><a
                    href="https://doi.org/10.1007/978-3-642-21303-8_23"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1007/978-3-642-21303-8_23</span></a>
            </p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Samrina F. <b>Assessment competence in Automated Education Systems with the
                        means of Cognitive Map of Diagnostic Knowledge</b>. <i>Interactive Systems and
                        Technologies: the Problem of Human-Computer Interaction</i>. Volume IX -
                    Collection of scientific papers. - Ulyanovsk: UlSTU, 2011. - p. 399-402. </span><a
                    href="https://www.researchgate.net/profile/Petr-Sosnin/publication/258516985_Interactive_Systems_and_Technologies/links/00463528853593d117000000/Interactive-Systems-and-Technologies.pdf#page=400"><span
                        lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://www.researchgate.net/profile/Petr-Sosnin/publication/258516985_Interactive_Systems_and_Technologies/links/00463528853593d117000000/Interactive-Systems-and-Technologies.pdf#page=400</span></a><span
                    lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'> </span></p>
            <p class=MsoListParagraphCxSpMiddle style='margin-top:15.75pt;margin-right:
0cm;margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V. <b>Intellectual Control Algorithm Interaction improvement by the Users
                        Education Process of the Automation Education Systems</b>. <i>International
                        Siberian Conference on Control and Communications (SIBCON)</i>. – Krasnoyarsk:
                    Siberian Federal University, 2011. – p. 143-146. </span><a
                    href="https://doi.org/10.1109/SIBCON.2011.6072615"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://doi.org/10.1109/SIBCON.2011.6072615</span></a>
            </p>
            <p class=MsoListParagraphCxSpLast style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:1.0cm;text-align:justify;line-height:normal;
background:white'><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>
            <h5 style='margin-top:7.9pt;margin-right:0cm;margin-bottom:7.9pt;margin-left:
0cm;text-indent:1.0cm;background:white'><span lang=EN-US style='font-size:14.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:windowtext'>2008 (1)</span></h5>
            <p class=MsoListParagraph style='margin-top:15.75pt;margin-right:0cm;
margin-bottom:7.9pt;margin-left:0cm;text-align:justify;text-indent:1.0cm;
line-height:normal;background:white'><span lang=EN-US style='font-size:11.5pt;
font-family:"Georgia",serif;color:#777777'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;
                    </span></span><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Uglev
                    V., Samrina F. <b>Using of possibilities in learning tests for
                        individualization of displaying material in electronic education courses</b>. <i>Modern
                        Techniques and Technologies: The fourteenth International Scientific and
                        Practical Conference of Students, Postgraduates and Young Scientists (MTT’2008)</i>,
                    Tomsk, Tomsk Polytechnic University. - Tomsk: TPU Press, 2008.- p. 96 - 100. </span><a
                    href="https://ieeexplore.ieee.org/abstract/document/4897509"><span lang=EN-US
                        style='font-size:14.0pt;font-family:"Times New Roman",serif'>https://ieeexplore.ieee.org/abstract/document/4897509</span></a>
            </p>
            <p class=MsoNormal style='text-indent:1.0cm'><span lang=EN-US style='font-size:
14.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></p>
        </div>
    </div>
</body>
</html>