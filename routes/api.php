<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Внешняя аутентификация, авторизация и регистрация
|--------------------------------------------------------------------------
*/

// Старый способ получения токена, удалить после перехода на использование /api/token/. . .
Route::post('/login', 'Auth\SanctumController@login');
Route::get('/auth', 'Auth\SanctumController@auth');

Route::prefix('token')->group(function () {
    Route::post('/login', 'Auth\SanctumController@login');

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/auth', 'Auth\SanctumController@auth');
        Route::delete('/revoke', 'Auth\SanctumController@revoke');
        Route::get('/check', 'Auth\SanctumController@check');
    });
});

/*
|--------------------------------------------------------------------------
| Доступ к основной базе данных AESFU
|--------------------------------------------------------------------------
*/

/**
 * Авторизированные пользователи
 */
Route::middleware('auth:sanctum')->group(function () {
    /** Authenticated USER */
    Route::prefix('auth')->group(function () {
        Route::put('/info', 'Database\mddb\AuthController@info');
        Route::put('/branch', 'Database\mddb\AuthController@branch');
        Route::put('/email', 'Database\mddb\AuthController@email');
        Route::put('/password', 'Database\mddb\AuthController@password');
        Route::get('/basket', 'Database\mddb\AuthController@basket');
    });

    /** BASKET */
    Route::prefix('basket')->group(function () {
        Route::get('/{id}', 'Database\mddb\BasketController@basket');
        Route::get('/{id}/result-html', 'Database\mddb\BasketController@resultHTML');
        Route::get('/{id}/score', 'Database\mddb\BasketController@score');
    });

    Route::post('/baskets', 'Database\mddb\BasketController@baskets');

    /** BRANCH */
    Route::prefix('branch')->group(function () {
        Route::get('/{id}', 'Database\mddb\BranchController@branch');
    });

    Route::post('/branches', 'Database\mddb\BranchController@branches');

    /** DISCIPLINE */
    Route::prefix('discipline')->group(function () {
        Route::get('/{id}', 'Database\mddb\DisciplineController@model');
    });

    Route::post('/disciplines', 'Database\mddb\DisciplineController@models');

    /** DOCUMENT */
    Route::prefix('document')->group(function () {
        Route::get('/{ulid}', 'Database\mddb\DocumentController@model');
        Route::delete('/{ulid}', 'Database\mddb\DocumentController@delete');
    });

    Route::prefix('lecture')->group(function () {
        Route::get('/{id}', 'Database\mddb\LectureController@model');
        Route::get('/{id}/document-html', 'Database\mddb\LectureController@documentHtml');
        Route::post('/', 'Database\mddb\LectureController@create');
        Route::delete('/{id}', 'Database\mddb\LectureController@delete');
        Route::post('/exists', 'Database\mddb\LectureController@exists');
    });

    Route::post('/lectures', 'Database\mddb\LectureController@models');

    /** QUEST */
    Route::prefix('quest')->group(function () {
        Route::get('/{id}', 'Database\mddb\QuestController@quest');
    });

    Route::post('/quests', 'Database\mddb\QuestController@quests');

    /** TEST */
    Route::prefix('test')->group(function () {
        Route::get('/{id}', 'Database\mddb\TestController@test');
        Route::get('/{id}/access', 'Database\mddb\TestController@hasAccess');
        Route::post('/{id}/complete', 'Database\mddb\TestController@complete');
    });

    Route::get('/tests', 'Database\mddb\TestController@tests');
    Route::get('/tests-to-pass', 'Database\mddb\TestController@testsToPass');

    /** USER */
    Route::prefix('user')->group(function () {
        Route::get('/{id}', 'Database\mddb\UserController@user');
        Route::get('/{id}/basket', 'Database\mddb\UserController@basket');
    });

    Route::get('/userstatus', 'Database\mddb\UserController@userStatus');
    Route::post('/users', 'Database\mddb\UserController@users');
    Route::post('/users/short', 'Database\mddb\UserController@shortusers');
});

/**
 * Гостевой доступ
 */
Route::prefix('guest')->group(function () {
    /** BASKET */
    Route::prefix('basket')->group(function () {
        Route::get('/{id}/profile', 'Database\mddb\BasketGuestController@profile');
        Route::get('/{id}/result-html', 'Database\mddb\BasketGuestController@resultHTML');
    });

    Route::get('/baskets', 'Database\mddb\BasketGuestController@baskets');

    /** BRANCH */
    Route::prefix('branch')->group(function () {
        Route::get('/{id}/users', 'Database\mddb\BranchGuestController@users');
    });

    Route::get('/branches', 'Database\mddb\BranchGuestController@branches');

    /** DISCIPLINE */
    Route::get('/disciplines', 'Database\mddb\DisciplineGuestController@models');

    /** OVERDISCIPLINE */
    Route::get('/over-discipline/profile', 'Database\mddb\OverDisciplineGuestController@profile');
});

/*
|--------------------------------------------------------------------------
| Доступ к данным научной конференции
|--------------------------------------------------------------------------
*/

/**
 * Привилегированный доступ
 */
Route::prefix('conf')->middleware(['auth:sanctum', 'permission:admin|console'])->group(function () {
    /** ARTICLE */
    Route::prefix('article')->group(function () {
        Route::get('/{id}', 'Database\conf\ArticleController@article');
        Route::put('/{id}/status', 'Database\conf\ArticleController@status');
        Route::put('/{id}/members', 'Database\conf\ArticleController@putMembers');
        Route::delete('/{id}', 'Database\conf\ArticleController@delete');
    });

    Route::post('/articles', 'Database\conf\ArticleController@articles');

    /** MEMBER */
    Route::prefix('member')->group(function () {
        Route::get('/{id}', 'Database\conf\MemberController@member');
        Route::put('/{id}', 'Database\conf\MemberController@update');
        Route::delete('/{id}', 'Database\conf\MemberController@delete');
    });

    Route::post('/members', 'Database\conf\MemberController@members');
});

/**
 * Гостевой доступ
 */
Route::prefix('guest/conf')->group(function () {
    /** Текст приглашения */
    Route::get('/appeal', 'Database\conf\AppealController@page');

    /** ARTICLE */
    Route::prefix('article')->group(function () {
        Route::post('/', 'Database\conf\ArticleGuestController@create');
        Route::get('/status', 'Database\conf\ArticleGuestController@status');
    });

    /** MEMBER */
    Route::prefix('member')->group(function () {
        Route::post('/', 'Database\conf\MemberGuestController@create');
    });

    Route::post('/members', 'Database\conf\MemberGuestController@members');

    /** SECTION */
    Route::post('/sections', 'Database\conf\SectionGuestController@sections');
});

/*
|--------------------------------------------------------------------------
| Логирование ошибок и исключений приложений
|--------------------------------------------------------------------------
*/

Route::middleware(['auth:sanctum'])->group(function () {
    /** Laravel */
    Route::middleware(['permission:develop'])->group(function () {
        Route::delete('/laravel/error/{id}', 'Database\log\LaravelController@delete');

        Route::delete('/laravel/errors', 'Database\log\LaravelController@clear');
    });

    Route::get('/laravel/errors', 'Database\log\LaravelController@errors');

    /** ReactJS */
    Route::middleware(['permission:develop'])->group(function () {
        Route::delete('/react/error/{id}', 'Database\log\ReactController@delete');

        Route::delete('/react/errors', 'Database\log\ReactController@clear');
    });

    Route::get('/react/errors', 'Database\log\ReactController@errors');
});

Route::post('/react/error', 'Database\log\ReactController@create');

/*
|--------------------------------------------------------------------------
| Дополнительные запросы отдельных приложений
|--------------------------------------------------------------------------
*/

Route::prefix('study')->group(function () {
    Route::post('/flm-builder', 'Other\DevelopController@flmBuilder');
});

/*
|--------------------------------------------------------------------------
| Запросы LMS CMKD -> MDDB
|--------------------------------------------------------------------------
*/

/** Создание, редактирование, удаление элементов образовательного процесса */
Route::middleware(['auth:sanctum', 'permission:develop|admin|console'])->group(function () {
    /**
     * Контроллер образовательного элемента, запросы класса EdElement, wbs, connectEdElement
     */
    Route::controller('Database\mddb\EdElementController')->group(function () {
        Route::post('/edelement/create', 'create');
        Route::put('/edelement/{id}', 'update');
        Route::delete('/edelement/{id}', 'delete');
        //connections btw ed elements
        Route::post('/eeconnect', 'createConnection');
        Route::put('/eeconnect/{id}', 'updateConnection');
        Route::delete('/eeconnect/{id}', 'deleteConnection');
        //--------------------------------------------------------------------------------------------//
        Route::post('/edelementlinkmethod', 'createMLink');
        Route::delete('edelementlinkmethod', 'deleteMLink');
        //--------------------------------------------------------------------------------------------//
        Route::post('/edelementlinktask', 'createTLink');
        Route::delete('edelementlinktask', 'deleteTLink');
        //--------------------------------------------------------------------------------------------//
        Route::post('/edelementlinkquest', 'createQLink');
        Route::delete('edelementlinkquest', 'deleteQLink');
        //--------------------------------------------------------------------------------------------//
        Route::post('/edelementlinkcomp', 'createCLink');
        Route::delete('/edelementlinkcomp', 'deleteCLink');
    });

    /**
     * Контроллер компетенций
     */
    Route::controller('Database\mddb\CompetenceController')->group(function () {
        Route::post('/competence/create', 'create');
        Route::put('/competence/{id}', 'update');
        Route::delete('/competence/{id}', 'delete');
    });

    /**
     * Контроллер методов, запросы method
     */
    Route::controller('Database\mddb\MethodController')->group(function () {
        Route::post('/method', 'create');
        Route::put('/method/{id}', 'update');
        Route::delete('method/{id}', 'delete');
    });

    /**
     * Контроллер задач, запросы task
     */
    Route::controller('Database\mddb\TaskController')->group(function () {
        Route::post('/task', 'create');
        Route::put('/task/{id}', 'update');
        Route::delete('task/{id}', 'delete');
    });

    /**
     * Контроллер личных предпочтений (цели при обучении)
     */
    Route::controller('Database\mddb\PersonalPreferenceController')->group(function () {
        Route::post('/personal', 'create');
        Route::put('/personal/{id}', 'update');
        Route::delete('personal', 'delete');
    });

    /**
     * Контроллер учебного курса - начатый пользователем
     */
    Route::controller('Database\mddb\UserEducationCourseController')->group(function () {
        Route::delete('/user/course/{id}', 'delete');
    });

    /**
     * Контроллер учебного курса - базовый
     */
    Route::controller('Database\mddb\EducationCourseController')->group(function () {
        Route::post('/course', 'create');
        Route::put('/course/{id}', 'update');
        Route::delete('/course/{id}', 'delete');
        Route::post('/courses', 'models');
    });

    /**
     * Контроллер образовательного профиля учащегося
     */
    Route::controller('Database\mddb\UserEducationProfileController')->group(function () {
        Route::post('/user/profile', 'create');
        Route::put('/user/profile/{id}', 'update');
        Route::post('/user/profiles', 'models');
        Route::delete('/user/profile/{id}', 'delete');
    });

    /**
     * Контроллер ОЦС (образовательный цифровой след)
     */
    Route::controller('Database\mddb\UserDigitalFootPrintController')->group(function () {
        Route::get('user/dfp/{id}', 'userdigitalfootprint');
    });

    /**
     * Контроллер файлов для учебного курса
     */
    Route::controller('Database\mddb\FileStorageController')->group(function () {
        Route::post('/filestorage', 'create');
        Route::delete('/filestorage/{id}', 'delete');
    });

});

/** Доступ учащегося к элементам образовательного процесса */
Route::middleware(['auth:sanctum'])->group(function () {
    /**
     * Контроллер образовательного элемента, запросы класса EdElement, wbs, connectEdElement
     */
    Route::controller('Database\mddb\EdElementController')->group(function () {
        Route::get('/edelement/{id}', 'edelement');
        Route::get('/edelement/didactic/wbs', 'edelementwbs');
        Route::get('/edelement/{id}/children', 'children');
        Route::get('/edelement/{id}/childrencount', 'childrenCount');
        Route::post('/edelements', 'models');
        Route::post('/edelements/short', 'leveledmodels');
    });

    /**
     * Контроллер компетенций
     */
    Route::controller('Database\mddb\CompetenceController')->group(function () {
        Route::get('/competence/{id}', 'competence');
        Route::get('/competence/{id}/children', 'children');
        Route::get('/competence/{id}/childrencount', 'childrenCount');
        Route::post('/competences', 'models');
    });

    /**
     * Контроллер методов, запросы method
     */
    Route::controller('Database\mddb\MethodController')->group(function () {
        Route::post('/methods', 'models');
    });

    /**
     * Контроллер задач, запросы task
     */
    Route::controller('Database\mddb\TaskController')->group(function () {
        Route::post('/tasks', 'Database\mddb\TaskController@models');
    });

    /**
     * Контроллер личных предпочтений (цели при обучении)
     */
    Route::controller('Database\mddb\PersonalPreferenceController')->group(function () {
        Route::post('/personals', 'models');
    });

    /**
     * Контроллер учебного курса - начатый пользователем
     */
    Route::controller('Database\mddb\UserEducationCourseController')->group(function () {
        Route::get('/user/course/{id}', 'usereducationcourse');
        Route::post('/user/course', 'create');
        Route::put('/user/course/{id}', 'update');
    });

    /**
     * Контроллер учебного курса - базовый
     */
    Route::controller('Database\mddb\EducationCourseController')->group(function () {
        Route::get('/course/{id}', 'educationcourse');
    });

    /**
     * Контроллер образовательного профиля учащегося
     */
    Route::controller('Database\mddb\UserEducationProfileController')->group(function () {
        Route::get('/user/profile/{id}', 'usereducationprofile');
        Route::post('/user/authprofile', 'authusereducationprofile');
        Route::get('/user/profile/{id}/dfp', 'userdigitalfootprints');
        Route::get('/user/profile/{id}/courses', 'courses');
    });

    /**
     * Контроллер ОЦС (образовательный цифровой след)
     */
    Route::controller('Database\mddb\UserDigitalFootPrintController')->group(function () {
        Route::post('/user/dfp', 'create');
        Route::post('/{profileIf}/dfp', 'profiledfp');
    });

    /**
     * Контроллер файлов для учебного курса
     */
    Route::controller('Database\mddb\FileStorageController')->group(function () {
        Route::get('/filestorage/{id}', 'filestorage');
        Route::post('/files', 'models');
    });

    /**
     * Иные запросы для уже имеющихся контроллеров
     */
    Route::controller('Database\mddb\TestController')->group(function () {
        Route::post('/tests', 'models');
        Route::get('/test/{id}/quests', 'questslist');
    });

    Route::controller('Database\mddb\BasketController')->group(function () {
        Route::post('/baskets/user', 'userBaskets');
        Route::post('/baskets/models', 'models');
        Route::post('baskets/result', 'result');
        Route::post('baskets/results', 'results');
        Route::post('baskets/form-preference/result', 'formresult');
        Route::post('baskets/form-preference/results', 'formresults');
        Route::post('baskets/results/united','unitedResults');
    });

    Route::controller('Database\mddb\BasketGuestController')->group(function () {
        Route::get('/basket/{id}/profile', 'CMKDprofile');
        Route::post('/basket/profiles', 'CMKDprofiles');
        Route::get('/basket/profile/user', 'SummaryProfile');
    });

    Route::controller('Database\mddb\DisciplineController')->group(function () {
        Route::get('/discipline/{id}/competences', 'competences');
        Route::get('/discipline/{id}/tests', 'tests');
    });

    Route::controller('Database\mddb\QuestController')->group(function () {
        Route::get('/quest/{id}/answers', 'answerslist');
    });
});